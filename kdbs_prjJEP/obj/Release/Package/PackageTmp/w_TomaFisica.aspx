﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="w_TomaFisica.aspx.cs" Inherits="kdbs_prjJEP.w_TomaFisica1" %>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Kohinor Web - JEP</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <%--SCRIPTS HEAD--%>
    
    <%--SCRIPTS HEAD--%>
</head>

<body id="page-top">
<form id="form1" runat="server">
    <%--SCRIPTS BODY--%>
   
    <%--SCRIPTS BODY--%>



  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
            <img src="Imagenes/Logo-Kohinor2.png" width="50px"/>
          <%--<i class="fas fa-laugh-wink"></i>--%>
        </div>
        <div class="sidebar-brand-text mx-2">KOHINOR <sup>V3.0</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw far fa-tasks"></i>
          <span>Despacho</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Componentes:</h6>
              <asp:LinkButton ID="lnbtnListadoDespacho" class="collapse-item" runat="server" OnClick="lnbtnListadoDespacho_Click">         
              <span>Listado Despacho</span></asp:LinkButton>
              <asp:LinkButton ID="lnbtnDespacho" class="collapse-item" runat="server" OnClick="lnbtnDespacho_Click">         
              <span>Despacho Artículos</span></asp:LinkButton>
          </div>
        </div>
      </li>  

        <!-- Divider -->
      <hr class="sidebar-divider my-0">

        <!-- Nav Item - Toma Física -->
      <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo1" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fal fa-edit"></i>  
          <span>Toma Física</span>
        </a>
        <div id="collapseTwo1" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Componentes:</h6>

              <asp:LinkButton ID="lnbtnListado" class="collapse-item" runat="server" OnClick="lnbtnListado_Click">         
              <span>Listado</span></asp:LinkButton>
             
              <asp:LinkButton ID="lnbtnTomaFisica" class="collapse-item" runat="server" OnClick="lnbtnTomaFisica_Click">
              <span>Toma Física </span></asp:LinkButton>
          </div>

        </div>
      </li>

        <!-- Divider -->
      <hr class="sidebar-divider my-0">

        <!-- Nav Item - Importaciones -->
      <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo2" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-envelope-open-text"></i>
          <span>Importaciones</span>
        </a>
        <div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Componentes:</h6>

              <asp:LinkButton ID="lnbtnImportListado" class="collapse-item" runat="server" OnClick="lnbtnImportListado_Click">         
              <span>Listado Import.</span></asp:LinkButton>
             
              <asp:LinkButton ID="lnbtnImportaciones" class="collapse-item" runat="server" OnClick="lnbtnImportaciones_Click">
              <span>Importaciones </span></asp:LinkButton>
          </div>

        </div>
      </li>

        <!-- Divider -->
      <hr class="sidebar-divider my-0">

        <!-- Nav Item - Transferencias -->
      <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo3" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-people-carry"></i>
          <span>Transferencias</span>
        </a>
        <div id="collapseTwo3" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Componentes:</h6>

              <asp:LinkButton ID="lnbtnTransferListado" class="collapse-item" runat="server" OnClick="lnbtnTransferListado_Click">
              <span>Listado Transfer.</span></asp:LinkButton>
             
              <asp:LinkButton ID="lnbtnTransferencias" class="collapse-item" runat="server" OnClick="lnbtnTransferencias_Click">
              <span>Transferencias </span></asp:LinkButton>
          </div>

        </div>
      </li>

        <!-- Divider -->
      <hr class="sidebar-divider my-0">

        <!-- Nav Item - Salida -->
      <li class="nav-item active">
        <%--<a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Despacho</span></a>--%>

        <asp:LinkButton ID="lnbtnSalida" class="nav-link" runat="server" OnClick="lnbtnSalida_Click">
           <i class="fas fa-fw far fa-sign-out-alt"></i>
           <span>Salida</span></asp:LinkButton>
      </li>      

      <!-- Divider -->
      <hr class="sidebar-divider">      

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button onclick="return false;" class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button onclick="return false;" id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
            
          <div class="d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <%--<input type="text" id="txtBuscar" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" runat="server" AutoPostBack="True">--%>
              <asp:TextBox ID="txtBuscar" runat="server" class="form-control bg-light border-0 small" placeholder="Buscar..." AutoPostBack="True"></asp:TextBox>
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </div>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
              <!-- Static Topbar Search for Mobile-->
                <%--<div class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">                    
                    <asp:TextBox ID="txtBuscarMobile" runat="server" class="form-control" placeholder="Search for..." AutoPostBack="True"></asp:TextBox>
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </div> --%>

              <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                    <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label>
                </span>
                <img class="img-profile rounded-circle" src="Imagenes/catUser.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">                     
                  <asp:HyperLink ID="hpkUsuario" class="dropdown-item" runat="server">                      
                      <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                      <asp:Label ID="lblUsuarioDrop" runat="server"></asp:Label>
                  </asp:HyperLink>
                
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
       <div class="container-fluid" style="display:none">

          <!-- BAJO CONSTRUCCION Error Text -->
          <div class="text-center">
            <div class="error mx-auto" data-text="000">! ! !</div>
            <p class="lead text-gray-800 mb-5">Página En Construcción</p>
            <p class="text-gray-500 mb-0">Parece que la página que busca no esta lista...</p>
          </div>
        </div>


        <div class="container-fluid"  >

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon"></asp:Label>
            <%--<h1 class="h3 mb-0 text-gray-800"><strong>Toma Física</strong></h1>--%>
            <%--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>--%>
              <%--<asp:Button ID="btnGuardar" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />--%>              
          </div>            

          
            <%--Seccion Textbox Toma Fisica--%>
          <div class="row">

            <!-- Area Chart -->
            <div class="col">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Toma Física</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Acciones:</div>
                        <asp:Button ID="btnDespachar" class="dropdown-item" runat="server" Text="&#9997; Despachar" OnClick="btnDespachar_Click" />
                        <asp:Button ID="btnPendiente" class="dropdown-item" runat="server" Text="💾 Grabar Pendiente" OnClick="btnPendiente_Click"/>
                        <%--<asp:Button ID="btnConsolidar" class="dropdown-item" runat="server" Text="&#9195; Consolidar" OnClick="btnConsolidar_Click" />--%>     
                      <div class="dropdown-divider"></div>
                        <asp:Button ID="btnGuardar" class="dropdown-item" runat="server" Text="&#9899; Guardar" OnClick="btnGuardar_Click" />
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">        
                    <div style="width: 100%; overflow: scroll">
                       <%-- <div class="row">--%>
                        <div class="row">
                            <div class="col-lg">
                                <b><center>-- Artículo --</center></b>
                                <asp:TextBox ID="txtCodart" class="form-control form-control-user" runat="server" placeholder="Código"></asp:TextBox>
                                <asp:TextBox ID="txtNomart" class="form-control form-control-user" runat="server" placeholder="Nombre"></asp:TextBox>
                                <div class="row">
                                    <div class="col-6">
                                        <asp:TextBox ID="txtCantid" class="form-control form-control-user" runat="server" TextMode="Number" placeholder="Cantidad"></asp:TextBox>                                        
                                    </div>
                                    <div class="col-6">
                                        <asp:TextBox ID="txtSaldo" class="form-control form-control-user" runat="server" TextMode="Number" placeholder="Saldo" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>                                
                            </div>                            
                            <div class="col-lg">
                                <b><center>-- Tarea --</center></b>
                                <asp:TextBox ID="txtNumegr" class="form-control form-control-user" runat="server" placeholder="N° Egreso" ReadOnly="true"></asp:TextBox>
                                <asp:TextBox ID="txtFecegr" class="form-control form-control-user" runat="server" ReadOnly="true"></asp:TextBox>
                                <%--<asp:TextBox ID="txtTittar" class="form-control form-control-user" runat="server" placeholder="Tittar" ReadOnly="true"></asp:TextBox>--%>
                                <asp:TextBox ID="txtNumtar" class="form-control form-control-user" runat="server" placeholder="Numtar" ReadOnly="true"></asp:TextBox>
                            </div>
                            <%--<div class="col-lg">
                                <b><center>-- Proveedor --</center></b>
                                <asp:TextBox ID="txtCodpro" class="form-control form-control-user" runat="server" placeholder="Codpro" ReadOnly="true"></asp:TextBox>
                                <asp:TextBox ID="txtNompro" class="form-control form-control-user" runat="server" placeholder="Nompro" ReadOnly="true"></asp:TextBox>
                                <asp:Button ID="btnProveedores" class="btn btn-primary" runat="server" Text="Buscar Proveedor" OnClick="btnProveedores_Click" />
                            </div>--%>
                        </div>
                        <%--</div>--%>
                        <%--<div class="form-row">
                            <div class="form-group col-md-6">
                                
                            </div>
                            <div class="form-group col-md-2">
                                
                            </div>
                        </div>
                        --%>
                    </div>
                  </div>
              </div>
            </div>

          </div>
            <%--Seccion Gridview Toma Fisica--%>
          <div class="row">

            <!-- Area Chart -->
            <div class="col">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">                  
                  <h6 class="m-0 font-weight-bold text-primary">Listado Artículos</h6>
                  
                </div>
                <!-- Card Body -->
                <div class="card-body">       
                    <div style="width: 100%; overflow: scroll">
                    <asp:GridView ID="grvRenglonesIng" runat="server"  AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" class="table table-sm table-dark">
                                    
                                            <Columns>
                                                <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                    <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="1%"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="codart" HeaderText="Código" SortExpression="codart" />
                                                <asp:BoundField DataField="cantid" HeaderText="Cantidad" SortExpression="cantid" DataFormatString="{0:n0}"  />
                                                <asp:BoundField DataField="nomart" HeaderText="Nombre" SortExpression="nomart" />                                                
                                            </Columns>
                                    
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                              
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#BBD9EF" ForeColor="#333333" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                             <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                            <AlternatingRowStyle BackColor="#F8FAFA" ForeColor="#284775" /> 
                    </asp:GridView>
                        
                     </div>
                  </div>
              </div>
            </div>

          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; KOHINOR 2020 Todos Los Derechos Reservados</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>  

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

    <!--i MODALS -->

    <!--i Modal Proveedores -->
<%--<div id="modalProveedores" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" >
                <h4 class="modal-title" >
                    Seleccione Proveedores
                </h4>
                <button type="button" class="close" data-dismiss="modal">
                    &times;</button>                
            </div>
            <div class="modal-body">
                <!-- CONTENIDO-->  
                
                <div class="form-row">
                    <asp:Label ID="Label1" runat="server" Text="Nombre"></asp:Label>
                    &nbsp;&nbsp;
                    <asp:TextBox ID="txtBuscarNompro" runat="server" class="form-control form-control-sm" Width="20%"></asp:TextBox>
                    <asp:Button ID="btnBuscarNompro" runat="server" Text="Buscar" class="btn btn-primary btn-sm" OnClick="btnBuscarNompro_Click"  />
                </div>

                <br />
                <div style = "width:100%; height: 350px; overflow: scroll;">
                    
                    <asp:GridView ID="grvProveedores" runat="server"  class="table table-sm table-dark" AutoGenerateColumns="False" DataSourceID="sqlProveedores" OnSelectedIndexChanged="grvProveedores_SelectedIndexChanged">
                   
                        <AlternatingRowStyle BackColor="#F8FAFA" ForeColor="#284775" /> 

                        <Columns>
                            <asp:CommandField ButtonType="Image" SelectImageUrl="~/Imagenes/Select-Hand.png" ShowSelectButton="True">
                            <ControlStyle Height="30px" />
                            <FooterStyle Height="30px" />
                            <HeaderStyle Height="30px" />
                            <ItemStyle Height="30px" />
                            </asp:CommandField>
                           
                            <asp:BoundField DataField="codpro" HeaderText="Código" SortExpression="codpro" />
                            <asp:BoundField DataField="nompro" HeaderText="Proveedor" SortExpression="nompro" />
                            <asp:BoundField DataField="tipind" HeaderText="Identificación" SortExpression="tipind" />
                            
                        </Columns>

                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#00445e" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#BBD9EF" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>                  

                </div>
                </div>          
            </div>
        </div>
     
    </div>--%>

    <!--f Modal Proveedores -->

    <!--i Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">¿Desea salir?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Seleccionar "Logout" abajo para salir de la sesión actual.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-primary" href="w_Login.aspx">Logout</a>
        </div>
      </div>
    </div>
  </div>
    <!--f Logout Modal-->

    <!--i Modal Observacion Popup -->
    <div id="modalObservacion" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" >
                    <h4 class="modal-title" >
                        Observación Toma Física
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>                
                </div>
                <div class="modal-body">
                    <!-- CONTENIDO-->                  
                    <div class="form-row">
                        <div style="width: 100%; overflow: scroll">
                            <%--<asp:TextBox ID="txtObservacion" runat="server" class="form-control form-control-sm" Width="100%" TextMode="MultiLine" Height="200" placeholder="*Descripción / razón del despacho. (Obligatorio llenar para poder guardar. Hasta 160 carácteres)"></asp:TextBox>--%>
                            <asp:GridView ID="grvObservacionArticulos" runat="server" class="table table-sm table-dark" AutoGenerateColumns="False">
                                <AlternatingRowStyle BackColor="#F8FAFA" ForeColor="#284775" />
                                <Columns>
                                    <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                        <ItemStyle Width="1%"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="codart" HeaderText="Código" SortExpression="codart" />
                                    <asp:BoundField DataField="cantid" HeaderText="Cantid" SortExpression="cantid" DataFormatString="{0:n0}"/>
                                    <asp:TemplateField HeaderText="Observación">
                                        <ItemTemplate>
                                            <div class="form-row">
                                                <asp:DropDownList ID="ddlgrvObservacion" CssClass="form-control" runat="server" DataSourceID="sqldsllenarddlObserv" DataTextField="nomcat" DataValueField="codcat"></asp:DropDownList>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                                <EditRowStyle BackColor="#999999" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#00445e" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#BBD9EF" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />

                            </asp:GridView>
                            *Indicar por que no se agregó los artículos de la lista.
                            <asp:Label ID="lblAlertaModal" runat="server" Text="" ForeColor="Maroon"></asp:Label>
                        </div>                        
                    </div>
                </div> 
                <div class="modal-footer">
                  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>                 
                  <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" class="btn btn-primary" OnClick="btnAceptar_Click"/>
                </div>
            </div>
        </div>
    </div>  
    <!--f Modal Observacion Popup -->


    <!--f MODALS -->

    <!--i SQLDATASOURCES-->
    <asp:SqlDataSource ID="sqldsRenglonesIng" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>" 
        SelectCommand="SELECT [codart], [nomart], [canfis] as cantid, [canpar] as canfac, [estdes], [candes] 
        FROM [renglonesingresos] 
        WHERE ([numfac] = @numfac)">
        <SelectParameters>
            <asp:SessionParameter Name="numfac" SessionField="gs_Numfac" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqldsllenarddlObserv" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>" SelectCommand="W_CRM_S_DDLOBSERVACION" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

    <%--<asp:SqlDataSource ID="sqlProveedores" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>" SelectCommand="SELECT TOP 50 [nompro], [tipind], [codpro] FROM [proveedores] WHERE (([nompro] LIKE '%' + @nompro + '%'))">
        <SelectParameters>
           <asp:SessionParameter Name="nompro" SessionField="gs_Nompro" Type="String" />
       </SelectParameters>
    </asp:SqlDataSource>--%>
    <!--f SQLDATASOURCES-->
</form>
</body>

</html>
