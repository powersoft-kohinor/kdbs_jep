﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjJEP
{
    public partial class w_TomaManual : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["gs_CodUs1"] != null)
                {
                    lblUsuario.Text = Session["gs_CodUs1"].ToString();
                    lblUsuarioDrop.Text = Session["gs_CodUs1"].ToString();
                }

                Session.Add("gs_ContVal", "0"); //para que al aplastar muchas veces solo se haga 1
                txtFecegr.Text = DateTime.Now.ToString("yyyy-MM-dd");
                ////SECUENCIA N° EGR
                txtNumegr.Text = f_CalcularSecuencia();
                //txtNumegr.Text = Session["gs_Numfac"].ToString();
                //txtTittar.Text = Session["gs_Tittar"].ToString();
                txtNumtar.Text = "Toma Física Manual";
                //txtNumtar.Text = Session["gs_Numtar"].ToString();

                DataTable dt = new DataTable();
                dt.Columns.Add("codart", typeof(string));
                dt.Columns.Add("nomart", typeof(string));
                dt.Columns.Add("cantid", typeof(decimal));
                dt.Columns.Add("preuni", typeof(decimal));
                dt.Columns.Add("coduni", typeof(string));
                Session.Add("gdt_Articulos", dt);
                grvRenglonesIng.DataSource = dt;
                grvRenglonesIng.DataBind();
            }

            if (IsPostBack)
            {
                if (!String.IsNullOrEmpty(txtBuscar.Text))
                {
                    string codart = txtBuscar.Text.Trim();
                    Session.Add("gs_Codart", codart);

                    clsArticulo objArticulo = f_BuscarArticulo(codart);
                    f_ActualizarCampos(objArticulo);
                    txtBuscar.Focus();
                }
            }
        }

        protected void lnbtnListadoDespacho_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_Inicio.aspx");
        }

        protected void lnbtnDespacho_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Despacho primero.');</script>");
        }

        protected void lnbtnSalida_Click(object sender, EventArgs e)
        {
            //Response.Redirect("w_Salida.aspx");
            Response.Redirect("w_Login.aspx");
        }

        protected void lnbtnListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TomaFisicaListado.aspx");
        }
        protected void lnbtnTomaFisica_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Toma Física primero.');</script>");
        }
        protected void lnbtnManual_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TomaManual.aspx");
        }

        protected void lnbtnImportListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_ImportListado.aspx");
        }

        protected void lnbtnImportaciones_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Importaciones primero.');</script>");
        }

        protected void lnbtnTransferListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TransferListado.aspx");
        }

        protected void lnbtnTransferencias_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Transferencias primero.');</script>");
        }

        //***i CALCULO SECUENCIAS**********************************************************
        public string f_CalcularSecuencia()
        {
            //ANTES DE LLENAR ENCABEZADO EGRESOS SE CALCULA EL NUMFAC
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_SECUENCIA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", Session["gs_CodEmp"].ToString());
            cmd.Parameters.AddWithValue("@codsec", "CP_ING");
            cmd.Parameters.AddWithValue("@sersec", Session["gs_SerSec"].ToString()); //del usuario en Login

            cmd.Parameters.Add("@sp_NumCom", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();
            string s_numfac = cmd.Parameters["@sp_NumCom"].Value.ToString();

            conn.Close();

            Session.Add("gs_NumFac", s_numfac);
            return s_numfac;
        }

        public void f_ActualizarSecuencia()
        {
            //ACTUALIZA LA SECUENCIA CUANDO EL USUARIO SELECCIONA GUARDAR.
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_SEC2]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", Session["gs_CodEmp"].ToString());
            cmd.Parameters.AddWithValue("@codsec", "CP_ING");
            cmd.Parameters.AddWithValue("@sersec", Session["gs_SerSec"].ToString()); //del usuario en Login

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*12. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }

        //***f CALCULO SECUENCIAS**********************************************************


        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
            {
                Session["gs_ContVal"] = "1";
                if (String.IsNullOrEmpty(txtCodpro.Text))
                {
                    Session["gs_ContVal"] = "0";
                    lblError.Text = "Debe seleccionar un proveedor antes de guardar.";
                }
                else
                {
                    f_insertarArticulos(); //est002=1 ...para q wladi sepa si fue parcial o completo

                    //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (VERDE)
                    //if (lblError.Text.Trim() == "") //control de errores
                    //{
                    //    f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numfac"].ToString(),
                    //        "1", "1", Session["gs_CodUs1"].ToString());
                    //}

                    //redireccion despues de guardar
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        Response.Redirect("w_TomaManual.aspx");
                    }
                }                
            }
        }

        public void f_actualizarObsev(string codart1, string codemp, string numfac, string observ)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_INGRESOS_REN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@observ", observ);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*14. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }
        public clsArticulo f_BuscarArticulo(string s_codart)
        {
            DataTable dt = (DataTable)Session["gdt_Articulos"];
            clsArticulo objArticulo = new clsArticulo();

            try
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row.Field<string>("codart").Trim().Equals(s_codart.Trim()))
                        {
                            objArticulo.Codart = s_codart;
                            string s_nomart = row.Field<string>("nomart");
                            objArticulo.Nomart = s_nomart;
                            int i_cantid = (int)row.Field<decimal>("cantid") + 1;
                            //actualiza cantid del dt
                            row["cantid"] = i_cantid;
                            objArticulo.Cantid = i_cantid;
                            objArticulo.Error = "CORRECTO";

                            Session["gdt_Articulos"] = dt;
                            grvRenglonesIng.DataSource = dt;
                            grvRenglonesIng.DataBind();
                            return objArticulo;
                        }
                    }
                    //si aun no hace return es que no hay el articulo en la lista
                    objArticulo = f_selectArticulo(Session["gs_CodEmp"].ToString(), s_codart);
                    if (!String.IsNullOrEmpty(objArticulo.Nomart))
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = objArticulo.Codart;
                        dr[1] = objArticulo.Nomart;
                        dr[2] = objArticulo.Cantid;
                        dr[3] = objArticulo.Preuni;
                        dr[4] = objArticulo.Coduni;
                        dt.Rows.Add(dr);

                        Session["gdt_Articulos"] = dt;
                        grvRenglonesIng.DataSource = dt;
                        grvRenglonesIng.DataBind();
                        return objArticulo;
                    }
                }
                else
                {
                    objArticulo = f_selectArticulo(Session["gs_CodEmp"].ToString(), s_codart);
                    if (!String.IsNullOrEmpty(objArticulo.Nomart))
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = objArticulo.Codart;
                        dr[1] = objArticulo.Nomart;
                        dr[2] = objArticulo.Cantid;
                        dr[3] = objArticulo.Preuni;
                        dr[4] = objArticulo.Coduni;
                        dt.Rows.Add(dr);

                        Session["gdt_Articulos"] = dt;
                        grvRenglonesIng.DataSource = dt;
                        grvRenglonesIng.DataBind();
                        return objArticulo;
                    } 
                }
            }
            catch (Exception ex)
            {
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "*02. " + DateTime.Now + " " + ex.Message;
                return objArticulo;
            }

            objArticulo.Codart = "-";
            objArticulo.Nomart = "-";
            objArticulo.Cantid = 0;
            objArticulo.Error = " *03. No se encontró artículo.";
            return objArticulo;
        }

        public clsArticulo f_selectArticulo(string s_codemp, string s_codart)
        {
            clsArticulo objArticulo = new clsArticulo();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_S_INGRESOS_ARTICULO]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", s_codemp);
            cmd.Parameters.AddWithValue("@CODART", s_codart);

            cmd.Parameters.Add("@s_Nomart", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@r_Preuni", SqlDbType.Decimal, 20).Direction = ParameterDirection.Output;
            cmd.Parameters["@r_Preuni"].Precision = 20;
            cmd.Parameters["@r_Preuni"].Scale = 2;
            cmd.Parameters.Add("@s_Coduni", SqlDbType.Char, 3).Direction = ParameterDirection.Output;
            try
            {
                //SqlDataAdapter sda = new SqlDataAdapter(cmd);
                //sda.Fill(dt);
                cmd.ExecuteNonQuery();
                objArticulo.Codart = s_codart;
                objArticulo.Nomart = cmd.Parameters["@s_Nomart"].Value.ToString().Trim();
                if (!String.IsNullOrEmpty(objArticulo.Nomart))
                {
                    objArticulo.Cantid = 1;
                    objArticulo.Preuni = !String.IsNullOrEmpty(cmd.Parameters["@r_Preuni"].Value.ToString().Trim()) ? Convert.ToDecimal(cmd.Parameters["@r_Preuni"].Value.ToString().Trim()) : 0;
                    objArticulo.Coduni = cmd.Parameters["@s_Coduni"].Value.ToString().Trim();
                    objArticulo.Error = "CORRECTO";
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "*07. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
            return objArticulo;
        }

        public void f_pintarGridview(string s_codart, int i_cantid) //pinta y llena el chkConfirmar
        {
            foreach (GridViewRow row in grvRenglonesIng.Rows)
            {

                //if ((int)decimal.Parse(row.Cells[2].Text.Trim()) == 0) //llena el chkConfirmar
                //{
                //    CheckBox rBoton = (CheckBox)row.Cells[4].FindControl("chkConfirmado");
                //    rBoton.Checked = true;
                //}
                string codart1 = row.Cells[1].Text.Trim();
                //string alterno = row.Cells[5].Text.Trim();

                if (row.Cells[1].Text.Trim().Equals(s_codart.Trim())) //pinta la fila con codart
                {
                    if (i_cantid > 0)
                        row.BackColor = Color.LightGreen;
                    else
                        row.BackColor = Color.Khaki;
                }
            }
        }
        
        protected void btnDespachar_Click(object sender, EventArgs e)
        {
            clsArticulo objArticulo = new clsArticulo();
            try
            {
                objArticulo.Codart = txtCodart.Text;
                objArticulo.Nomart = txtNomart.Text;
                objArticulo.Cantid = (int)decimal.Parse(txtSaldo.Text);
                objArticulo.Error = "";
                DataTable dt = (DataTable)Session["gdt_Articulos"];
                string s_codart = txtCodart.Text.Trim();

                int i_cantidDesapachar = 1;
                if (!txtCantid.Text.Equals("")) //si deja vacio txtCantid se asume que es 1
                {
                    i_cantidDesapachar = int.Parse(txtCantid.Text.ToString());
                }

                foreach (DataRow row in dt.Rows)
                {
                    if (row.Field<string>("codart").Trim().Equals(s_codart.Trim()))
                    {
                        //if ((int)row.Field<decimal>("cantid") < i_cantidDesapachar) //si ingresa cantidad mayor que saldo existente
                        //{
                        //    txtCantid.Text = "1";
                        //    objArticulo.Error = "*10. Cantidad ingresada es superior a Saldo. Despacho no realizado.";
                        //    f_ActualizarCampos(objArticulo);
                        //}
                        //else //cantidad correcta
                        //{

                        //}

                        objArticulo.Codart = s_codart;
                        string s_nomart = row.Field<string>("nomart");
                        objArticulo.Nomart = s_nomart;
                        int i_cantid = (int)row.Field<decimal>("cantid") + i_cantidDesapachar;

                        //actualiza el dt
                        row["cantid"] = i_cantid;

                        objArticulo.Cantid = i_cantid;
                        objArticulo.Error = "CORRECTO";

                        Session["gdt_Articulos"] = dt;
                        //grvRenglonespedpro.Columns[5].Visible = true; //para q f_MostrarCodAlterno() funcione
                        grvRenglonesIng.DataSource = dt;
                        grvRenglonesIng.DataBind();

                        //f_MostrarCodAlterno(); //PARA MOSTRAR CODIGO ALTERNO
                        //f_pintarGridview(s_codart, i_cantid, s_desart);
                        //grvRenglonespedpro.Columns[5].Visible = false; //ocultar columna extra...DEBE IR AQUI PARA QUE COLORES DE GRV FUNCIONEN
                        f_ActualizarCampos(objArticulo);
                    }
                }
                //aqui poner objArticulo.Codart = "-"; .....
            }
            catch (Exception ex)
            {
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "*11. " + DateTime.Now + " " + ex.Message;
                f_ActualizarCampos(objArticulo);
            }
        }

        public void f_ActualizarCampos(clsArticulo objArticulo)
        {
            txtCodart.Text = objArticulo.Codart;
            txtNomart.Text = objArticulo.Nomart;
            txtSaldo.Text = objArticulo.Cantid.ToString();
            if (!objArticulo.Error.Equals("CORRECTO"))
            {
                lblError.Text = "*01. " + DateTime.Now + objArticulo.Error;
            }
            else
            {
                lblError.Text = "";
            }
            txtBuscar.Text = "";
        }

        public void f_actualizarColoresGrv(string codemp, string numfac, string est001,
           string est002, string usu001)
        {
            //para actualizar los colores del enbabezadopedpro
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_COLOR_INGRESOS_ENC]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@est001", est001);
            cmd.Parameters.AddWithValue("@est002", est002);
            cmd.Parameters.AddWithValue("@usu001", usu001);
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*07. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }


        //***i PARA LLENAR INGRESOS**********************************************************
        public void f_insertarArticulos()
        {
            lblError.Text = "";
            string codemp = Session["gs_CodEmp"].ToString();
            string numfac = f_CalcularSecuencia().Trim();
            string codpro = txtCodpro.Text;
            string codalm = Session["gs_CodAlm"].ToString();
            string codusu = Session["gs_CodUs1"].ToString(); //se inserta tambien en usuing
            string tiptra = ""; //Session["gs_Tiptar"].ToString(); //???
            string numtra = ""; //Session["gs_Numtar"].ToString(); //???
            //string codcli = Session["gs_Codcli"].ToString();
            //string nomcli = Session["gs_Nomcli"].ToString();            
            //string codven = Session["gs_CodVen"].ToString();            
            string sersec = Session["gs_SerSec"].ToString();
            string codsuc = Session["gs_CodSuc"].ToString();
            string observ = txtObserv.Text;
            string s_enc = "";

            s_enc = f_llenarEncabezadoIngresos(codemp, numfac, codpro, codalm, codusu,
                tiptra, numtra, sersec, codsuc, codusu, observ); //se llama al SP

            if (s_enc.Equals("")) //si no hubo error al insertar en el encabezado
            {
                DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];
                long numren1 = 0;
                long numite1 = 0;
                foreach (DataRow row in dtArticulos.Rows)
                {
                    //Session.Add("gs_Codart", row.Field<string>("codart"));
                    string codart1 = row.Field<string>("codart");
                    numren1 += 1;
                    numite1 += 1;
                    string nomart1 = row.Field<string>("nomart");
                    string coduni1 = row.Field<string>("coduni");
                    decimal preuni1 = row.Field<decimal>("preuni");
                    //string ubifis1 = row.Field<string>("ubifis");
                    decimal cantid1 = row.Field<decimal>("cantid");

                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        f_llenarRenglonesIngresos(codart1, codemp, numfac, numren1, numite1, nomart1, coduni1,
                        preuni1, cantid1);
                    }
                }
            }
        }

        public string f_llenarEncabezadoIngresos(string codemp, string numfac, string codpro,
            string codalm, string codusu, string tiptra, string numtra, string sersec,
            string codsuc, string usuing, string observ)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_INGRESOS_ENC]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@codpro", codpro);
            cmd.Parameters.AddWithValue("@codalm", codalm);
            cmd.Parameters.AddWithValue("@codusu", codusu);
            cmd.Parameters.AddWithValue("@tiptra", tiptra);
            cmd.Parameters.AddWithValue("@numtra", numtra);
            cmd.Parameters.AddWithValue("@sersec", sersec);
            cmd.Parameters.AddWithValue("@codsuc", codsuc);
            cmd.Parameters.AddWithValue("@usuing", usuing);
            cmd.Parameters.AddWithValue("@observ", observ);
            try
            {
                cmd.ExecuteNonQuery();
                conn.Close();
                //actualiza la secuencia
                f_ActualizarSecuencia();
                
                return ""; //se inserto correctamente
            }
            catch (Exception ex)
            {
                conn.Close();
                lblError.Text = "*04. " + DateTime.Now + ex.Message;
                return "*04. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
        }


        public void f_llenarRenglonesIngresos(string codart1, string codemp, string numfac, long numren1,
            long numite1, string nomart1, string coduni1, decimal preuni1, decimal cantid1)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_INGRESOS_REN_INSERT]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@numren", numren1);
            cmd.Parameters.AddWithValue("@numite", numite1);
            cmd.Parameters.AddWithValue("@nomart", nomart1);
            cmd.Parameters.AddWithValue("@coduni", coduni1);
            cmd.Parameters.AddWithValue("@cantid", cantid1);
            cmd.Parameters.AddWithValue("@preuni", preuni1);
            //cmd.Parameters.AddWithValue("@ubifis", ubifis1);
            cmd.Parameters.AddWithValue("@codalm", Session["gs_CodAlm"].ToString());

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*05. " + DateTime.Now + ex.Message;
                //throw ex;
            }
            conn.Close();
        }

        //***f PARA LLENAR INGRESOS**********************************************************


        //***i PARA PROVEEDORES**********************************************************
        protected void grvProveedores_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvProveedores.SelectedRow;
            string s_codpro = fila.Cells[1].Text.Trim();
            string s_nompro = fila.Cells[2].Text.Trim();
            txtCodpro.Text = s_codpro;
            txtNompro.Text = s_nompro;
            lblError.Text = "";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$(function() { $('#modalProveedores').modal('hide'); });</script>", false);
        }

        protected void btnBuscarNompro_Click(object sender, EventArgs e)
        {
            Session["gs_Nompro"] = txtBuscarNompro.Text.Equals("") ?  "%" : txtBuscarNompro.Text.Trim();
            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "$('#modalProveedores').modal('show')", true);
        }

        protected void btnProveedores_Click(object sender, EventArgs e)
        {
            Session.Add("gs_Nompro", "%");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$(function() { $('#modalProveedores').modal('show'); });</script>", false);
        }

        //***f PARA PROVEEDORES**********************************************************
    }
}