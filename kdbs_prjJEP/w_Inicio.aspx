﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="w_Inicio.aspx.cs" Inherits="kdbs_prjJEP.w_Inicio" %>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Kohinor Web - JEP</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">
<form id="form1" runat="server">
  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
            <img src="Imagenes/Logo-Kohinor2.png" width="50px"/>
          <%--<i class="fas fa-laugh-wink"></i>--%>
        </div>
        <div class="sidebar-brand-text mx-2">KOHINOR <sup>V3.0</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw far fa-tasks"></i>
          <span>Despacho</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Componentes:</h6>
              <asp:LinkButton ID="lnbtnListadoDespacho" class="collapse-item" runat="server" OnClick="lnbtnListadoDespacho_Click">         
              <span>Listado Despacho</span></asp:LinkButton>
              <asp:LinkButton ID="lnbtnDespacho" class="collapse-item" runat="server" OnClick="lnbtnDespacho_Click">         
              <span>Despacho Artículos</span></asp:LinkButton>
          </div>
        </div>
      </li>  

        <!-- Divider -->
      <hr class="sidebar-divider my-0">

        <!-- Nav Item - Toma Física -->
      <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo1" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fal fa-edit"></i>  
          <span>Toma Física</span>
        </a>
        <div id="collapseTwo1" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Componentes:</h6>

              <asp:LinkButton ID="lnbtnListado" class="collapse-item" runat="server" OnClick="lnbtnListado_Click">         
              <span>Listado Toma F.</span></asp:LinkButton>
             
              <asp:LinkButton ID="lnbtnTomaFisica" class="collapse-item" runat="server" OnClick="lnbtnTomaFisica_Click">
              <span>Toma Física </span></asp:LinkButton>
          </div>

        </div>
      </li>

        <!-- Divider -->
      <hr class="sidebar-divider my-0">

        <!-- Nav Item - Toma Manual -->
      <li class="nav-item active">
        <asp:LinkButton ID="lnbtnTomaManual" class="nav-link" runat="server" OnClick="lnbtnManual_Click">
           <i class="fas fa-fw fal fa-user-edit"></i>
          <span>Toma Manual</span>
        </asp:LinkButton>

          <!-- Divider -->
      <hr class="sidebar-divider my-0">

        <!-- Nav Item - Importaciones -->
      <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo2" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-envelope-open-text"></i>
          <span>Importaciones</span>
        </a>
        <div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Componentes:</h6>

              <asp:LinkButton ID="lnbtnImportListado" class="collapse-item" runat="server" OnClick="lnbtnImportListado_Click">         
              <span>Listado Import.</span></asp:LinkButton>
             
              <asp:LinkButton ID="lnbtnImportaciones" class="collapse-item" runat="server" OnClick="lnbtnImportaciones_Click">
              <span>Importaciones </span></asp:LinkButton>
          </div>

        </div>
      </li>

        <!-- Divider -->
      <hr class="sidebar-divider my-0">

        <!-- Nav Item - Transferencias -->
      <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo3" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-people-carry"></i>
          <span>Transferencias</span>
        </a>
        <div id="collapseTwo3" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Componentes:</h6>

              <asp:LinkButton ID="lnbtnTransferListado" class="collapse-item" runat="server" OnClick="lnbtnTransferListado_Click">
              <span>Listado Transfer.</span></asp:LinkButton>
             
              <asp:LinkButton ID="lnbtnTransferencias" class="collapse-item" runat="server" OnClick="lnbtnTransferencias_Click">
              <span>Transferencias </span></asp:LinkButton>
          </div>

        </div>
      </li>

        <!-- Divider -->
      <hr class="sidebar-divider my-0">

        <!-- Nav Item - Salida -->
      <li class="nav-item active">
        <%--<a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Despacho</span></a>--%>

        <asp:LinkButton ID="lnbtnSalida" class="nav-link" runat="server" OnClick="lnbtnSalida_Click">
           <i class="fas fa-fw far fa-sign-out-alt"></i>
           <span>Salida</span></asp:LinkButton>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">      

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button onclick="return false;" class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button onclick="return false;" id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <div class="d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Buscar..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </div>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <%--<!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>--%>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                    <asp:Label ID="lblUsuario" runat="server"></asp:Label>
                </span>
                <img class="img-profile rounded-circle" src="Imagenes/catUser.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">                     
                  <asp:HyperLink ID="hpkUsuario" class="dropdown-item" runat="server">                      
                      <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                      <asp:Label ID="lblUsuarioDrop" runat="server"></asp:Label>
                  </asp:HyperLink>
                
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
              <asp:Label ID="lblError" runat="server" Text="" ForeColor="Maroon"></asp:Label>
            <%--<h1 class="h3 mb-0 text-gray-800"><strong>Toma Física</strong></h1>--%>
            <%--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>--%>
              
          </div>

          

          <div class="row">

            <!-- Area Chart -->
            <div class="col">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Listado Despachos</h6>
                  
                </div>
                <!-- Card Body -->
                


                  <div class="card-body">
                  <div class="chart-area" style="width: 100%; height: 400px; overflow: scroll">
                    <%--K<canvas id="">--%>

                         <asp:GridView ID="grvEncabezadopedpro" runat="server"  AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" class="table table-sm table-dark" DataSourceID="sqldsEncabezadopedpro" OnSelectedIndexChanged="grvEncabezadopedpro_SelectedIndexChanged" OnRowDataBound="grvEncabezadopedpro_RowDataBound">
                                    
                                            <Columns>
                                                <asp:CommandField ButtonType="Image" SelectImageUrl="~/Imagenes/Select-Hand.png" ShowSelectButton="True">
                                                    <ControlStyle Width="30px"/>
                                                </asp:CommandField>
                                                <asp:TemplateField HeaderText="N°" ItemStyle-Width="1%">
                                                    <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="1%"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="numtra" HeaderText="Num." SortExpression="numtra" />
                                                <asp:BoundField DataField="fectra" HeaderText="Fecha" SortExpression="fectra" DataFormatString="{0:dd-MM-yyyy}"/>
                                                <asp:BoundField DataField="nomcli" HeaderText="Nombre" SortExpression="nomcli" />
                                                
                                                <asp:BoundField DataField="est001" HeaderText="est001" SortExpression="est001" />
                                                <asp:BoundField DataField="est002" HeaderText="est002" SortExpression="est002" />
                                                <asp:BoundField DataField="observ" HeaderText="Observ." SortExpression="observ" />
                                            </Columns>
                                    
                                            <EditRowStyle BackColor="#999999" />
                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                              
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#BBD9EF" ForeColor="#333333" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                             <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                            <AlternatingRowStyle BackColor="#F8FAFA" ForeColor="#284775" /> 
                    </asp:GridView>
                    
                    <%--</canvas>--%>
                  </div>
                </div>


              </div>
            </div>

          </div>


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; KOHINOR 2020 Todos Los Derechos Reservados</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">¿Desea salir?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Seleccionar "Logout" abajo para salir de la sesión actual.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-primary" href="w_Login.aspx">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

  <!--i SQL DATASOURCES-->
                    <asp:SqlDataSource ID="sqldsEncabezadopedpro" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>"
                        SelectCommand="W_CRM_S_PEDPRO_ENC" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:SessionParameter Name="codus1" SessionField="gs_CodUs1" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                      <asp:SqlDataSource ID="sqldsEncPedproFull" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>" SelectCommand="SELECT [tiptra], [numtra], [codcli], [nomcli], [codven], [codusu], [observ], [est002]  FROM [encabezadopedpro] WHERE ([numtra] = @numtra)">
                          <SelectParameters>
                              <asp:SessionParameter Name="numtra" SessionField="gs_Numtra" Type="String" />
                          </SelectParameters>
                      </asp:SqlDataSource>

  <!--f SQL DATASOURCES-->


</form>
</body>

</html>




<%--<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
    </form>
</body>
</html>--%>

