﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace kdbs_prjJEP
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session.Add("gs_Error", "");
            //esta variable (gs_Error) puede causar problemas en Application_Error
            //ver https://stackoverflow.com/questions/6940425/session-state-is-not-available-in-this-context

            Session.Add("gs_PagPre", ""); //para almacenar la pagina previa...usada en error 404
        }

        //PARA ERRORES DEL SERVIDOR...NO HTTP....TAMBIEN SE ESTA USANDO EL customErrors DEL web.config
        protected void Application_Error(object sender, EventArgs e)
        {
            // An error has occured on a .Net page.
            Exception exception = Server.GetLastError();
            //var serverError = Server.GetLastError() as HttpException;
            if (exception != null)
            {
                //string borrar = exception.Message;
                HttpContext context = HttpContext.Current;
                if (context != null)
                {
                    if (context.Session != null)
                    {
                        Session["gs_Error"] = exception.InnerException.ToString();
                        //Server.ClearError(); //se comento por que no permitia que customErrors redireccione
                    }
                    else //para errores en pag aspx (diseño) ó 'Session state is not available in this context.'
                    {
                        string s_errorMessage = exception.Message;
                        Response.Redirect("w_Error.aspx?ErrorDesign='" + s_errorMessage + "'");
                        //Server.ClearError();
                    }

                    //if (context != null && context.Session != null)
                    //{
                    //    Session["gs_Error"] = exception.InnerException.ToString();
                    //}

                }                

                //if (serverError.GetHttpCode() == 404)
                //{
                //    //Server.ClearError();
                //    //Server.Transfer("404.html");
                //}
            }
        }
    }
}