﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjJEP
{
    public partial class w_Importaciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["gs_CodUs1"] != null)
                {
                    lblUsuario.Text = Session["gs_CodUs1"].ToString();
                    lblUsuarioDrop.Text = Session["gs_CodUs1"].ToString();
                }

                Session.Add("gs_ContVal", "0"); //para que al aplastar muchas veces solo se haga 1
                txtFecegr.Text = DateTime.Now.ToString("yyyy-MM-dd");
                ////SECUENCIA N° EGR
                //txtNumegr.Text = f_CalcularSecuencia();
                txtNumegr.Text = Session["gs_Numfac"].ToString();
                //txtTittar.Text = Session["gs_Tittar"].ToString();
                txtNumtar.Text = Session["gs_Numtar"].ToString();

                //PARA LLENAR grvRenglonespedpro
                DataSourceSelectArguments args = new DataSourceSelectArguments(); //para pasar del SqlDataSource1 a una DataTable
                DataView view = (DataView)sqldsRenglonesImp.Select(args);            //para pasar del SqlDataSource1 a una DataTable
                DataTable dt = view.ToTable(); //en esta dataTable esta el pedido seleccionado... (con los datos de este se llenara el encabezadoegresos)
                //para agregar saldo (cantid original)
                dt.Columns.Add("saldo", typeof(decimal));
                foreach (DataRow row in dt.Rows)
                {
                    row["saldo"] = row.Field<decimal>("cantid"); //guarda el total de cada articulo en columna saldo

                    if (!DBNull.Value.Equals(row["canfac"])) //para saber si canfac es NULL
                    {
                        row["cantid"] = row.Field<decimal>("canfac"); //si han hecho despacho parcial el valor de canfac debe guardarse en cantid
                        row["saldo"] = row["cantid"];
                    }
                }

                Session.Add("gdt_Articulos", dt);
                grvRenglonesImp.DataSource = dt;
                grvRenglonesImp.DataBind();

                //la cantid puede ser 0 cuando hayan realizado un despacho parcial
                //para esto se llena el chk y se pinta de verde apenaz ingrese a Despacho.aspx
                int rowIndex = 0;
                foreach (GridViewRow row in grvRenglonesImp.Rows)
                {
                    CheckBox rBoton = (CheckBox)row.Cells[5].FindControl("chkConfirmado");
                    rBoton.Enabled = true;
                    try
                    {
                        if (!DBNull.Value.Equals(dt.Rows[rowIndex]["observ"])) //si observ no es NULL
                        {
                            rBoton.Checked = true;
                            DropDownList ddlgrvObserv = (DropDownList)row.Cells[5].FindControl("ddlgrvObservRen");
                            ddlgrvObserv.SelectedValue = dt.Rows[rowIndex]["observ"].ToString().Trim();
                            ddlgrvObserv.DataBind();
                        }
                    }
                    catch (Exception ex)
                    {
                        lblError.Text = "*20. " + DateTime.Now + " " + ex.Message;
                    }

                    //para poner color azul en caso de que haya sido grabado pendiente
                    string estdes = "";
                    if (!DBNull.Value.Equals(dt.Rows[rowIndex]["estdes"])) //si estdes no es NULL
                    {
                        estdes = dt.Rows[rowIndex].Field<string>("estdes");
                    }

                    if (estdes.Equals("1"))
                    {
                        row.BackColor = Color.DodgerBlue;
                        if ((int)decimal.Parse(row.Cells[3].Text.Trim()) > (int)decimal.Parse(row.Cells[2].Text.Trim()))
                        {
                            row.BackColor = Color.LightCoral;
                        }
                    }

                    if ((int)decimal.Parse(row.Cells[2].Text.Trim()) - (int)decimal.Parse(row.Cells[3].Text.Trim()) == 0) //llena el chkConfirmar
                    {
                        row.BackColor = Color.LightGreen;
                    }
                    //else if ((int)decimal.Parse(row.Cells[3].Text.Trim()) > 0)
                    //{
                    //    row.BackColor = Color.Khaki;
                    //}

                    //PARA MOSTRAR EL CODIGO ALTERNO EN LUGAR DE CODART....SOLO SI NO ES NULL
                    if (row.Cells[6].Text != "&nbsp;") //para saber si desart es NULL
                    {
                        string s_aux = row.Cells[1].Text; //PARA PASAR desart A Cells[1] y codart  A Cells[5]
                        row.Cells[1].Text = row.Cells[6].Text; //Muestra desart en Código
                        row.Cells[6].Text = s_aux;
                    }
                    rowIndex++;
                }
                grvRenglonesImp.Columns[6].Visible = false; //esconde la columna de desart

            }

            if (IsPostBack)
            {
                if (!txtBuscar.Text.Equals(""))
                {
                    //validacion para borrar 001 al final de codart...en caso de que tenga 001
                    string s_result = txtBuscar.Text.Trim();
                    try
                    {
                        //int index001 = s_result.LastIndexOf("001");
                        //int index0001 = s_result.LastIndexOf("0001");

                        //if (index0001!=-1)
                        //{
                        //    s_result = s_result.Remove(index0001);

                        //}
                        //else if (index001 != -1)
                        //{
                        //    s_result = s_result.Remove(index001);
                        //}
                        //else
                        //{
                        //    s_result = txtBuscar.Text;
                        //}

                        string s_tresdigitos = txtBuscar.Text.Substring(txtBuscar.Text.Length - 3);
                        string s_cuatrodigitos = txtBuscar.Text.Substring(txtBuscar.Text.Length - 4);
                        if (s_tresdigitos.Equals("001") && !s_cuatrodigitos.Equals("0001"))
                            s_result = s_result.Substring(0, s_result.Length - 3);
                        else if (s_cuatrodigitos.Equals("0001"))
                            s_result = s_result.Substring(0, s_result.Length - 4);
                        else
                            s_result = txtBuscar.Text;


                        if (s_result.Length>10)
                        {

                        }                     


                    }
                    catch (Exception ex)
                    {
                        s_result = txtBuscar.Text;
                    }


                    //string codart = txtBuscar.Text.Trim();
                    Session.Add("gs_Codart", s_result);
                    if (!s_result.Equals(""))
                    {
                        clsArticulo objArticulo = f_BuscarArticulo(s_result);
                        f_ActualizarCampos(objArticulo);
                        txtBuscar.Focus();
                    }
                }
            }
        }

        protected void lnbtnListadoDespacho_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_Inicio.aspx");
        }

        protected void lnbtnDespacho_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Despacho primero.');</script>");
        }

        protected void lnbtnSalida_Click(object sender, EventArgs e)
        {
            //Response.Redirect("w_Salida.aspx");
            Response.Redirect("w_Login.aspx");
        }

        protected void lnbtnListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TomaFisicaListado.aspx");
        }
        protected void lnbtnTomaFisica_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Toma Física primero.');</script>");
        }

        protected void lnbtnImportListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_ImportListado.aspx");
        }

        protected void lnbtnImportaciones_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_Importaciones.aspx");
        }

        protected void lnbtnTransferListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TransferListado.aspx");
        }

        protected void lnbtnTransferencias_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Transferencias primero.');</script>");
        }

        public void f_MostrarCodAlterno()
        {
            //grvRenglonespedpro.Columns[5].Visible = true;
            //PARA MOSTRAR EL CODIGO ALTERNO EN LUGAR DE CODART....SOLO SI NO ES NULL
            foreach (GridViewRow row in grvRenglonesImp.Rows)
            {
                if (row.Cells[6].Text != "&nbsp;") //para saber si desart es NULL
                {
                    string s_aux = row.Cells[1].Text; //PARA PASAR desart A Cells[1] y codart  A Cells[5]
                    row.Cells[1].Text = row.Cells[6].Text; //Muestra desart en Código
                    row.Cells[6].Text = s_aux;
                }
            }
        }

        //***i LLENAR GRVRENGLONESPEDPRO**********************************************************

        public int f_CanTot() //PARA CALCULAR CANTID TOTAL DE grvRenglonespedpro...(PARA VALIDACION DE btnGuardar)
        {
            //1 -->PARCIAL (ALGUNO TIENE CANTID 0)
            //0 -->COMPLETO(NINGUNO ESTA CON CANTID 0)
            int i_cantot = 0;
            foreach (GridViewRow row in grvRenglonesImp.Rows)
            {
                if (decimal.Parse(row.Cells[2].Text.Trim()) == 0)
                {
                    i_cantot = 1;
                }
                //i_cantot = i_cantot + (int)decimal.Parse(row.Cells[2].Text.Trim());
            }
            return i_cantot;
        }


        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (f_CanTot() == 0) //verifica si todos los cantid estan en 0....REALIZO PARCIAL PERO TODOS ESTABAN EN CERO....SE PONE EN VERDE
            {
                if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
                {
                    Session["gs_ContVal"] = "1";
                    f_insertarArticulos("btnGuardar", "1"); //est002=1 ...para q wladi sepa si fue parcial o completo

                    //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (VERDE)
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numfac"].ToString(),
                            "1", "1", Session["gs_CodUs1"].ToString());
                    }

                    //redireccion despues de guardar
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        Response.Redirect("w_ImportListado.aspx");
                    }
                }
            }
            else //GRABADO PARCIAL NORMAL
            {
                btnGuardarParcial_Click();
            }
        }

        protected void btnAceptar_Click(object sender, EventArgs e) //para guardarParcial
        {
            if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
            {
                Session["gs_ContVal"] = "1";
                f_insertarArticulos("btnAceptar", "2"); //est002=2 ...para q wladi sepa si fue parcial o completo

                //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (LILA)
                if (lblError.Text.Trim() == "") //control de errores
                {
                    f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numfac"].ToString(),
                        "1", "2", Session["gs_CodUs1"].ToString());
                }

                //redireccion despues de guardar
                if (lblError.Text.Trim() == "") //control de errores
                {
                    Response.Redirect("w_ImportListado.aspx");
                }
            }
        }

        protected void btnGuardarParcial_Click()
        {
            lblAlertaModal.Text = "";
            int rowIndex = 0;
            DataTable dtObservacionArticulos = (DataTable)Session["gdt_Articulos"];

            //PARA MOSTRAR ROW DE LOS ARTICULOS QUE NO SE VEN A INSERTAR (SU CANTID NO ES CERO)
            //for (int i = 0; i < dtObservacionArticulos.Rows.Count; i++)
            //{

            //    if (cantidadDes == 0) //si sobran articulos los muestra (si su cantidadDes es 0)
            //    {
            //        dtObservacionArticulos.Rows.RemoveAt(i);
            //    }
            //}

            grvObservacionArticulos.DataSource = dtObservacionArticulos;
            grvObservacionArticulos.DataBind();

            foreach (GridViewRow row in grvObservacionArticulos.Rows)
            {

                //PARA MOSTRAR ROW DE LOS ARTICULOS QUE NO SE VEN A INSERTAR (SU CANTID ES CERO)
                decimal cantidadDes = dtObservacionArticulos.Rows[rowIndex].Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                //decimal saldo = dt.Rows[rowIndex].Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                //decimal cantid1 = saldo - cantidadDes; //articulos despachados

                if (cantidadDes == 0) // muestra (si su cantidadDes  es 0)
                {
                    grvObservacionArticulos.Rows[rowIndex].Visible = true;
                }
                else
                {
                    grvObservacionArticulos.Rows[rowIndex].Visible = false;
                }
                rowIndex++;
            }

            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "$('#modalObservacion').modal('show')", true);
            //de aqui va al btnAceptar
        }

        public void f_insertarArticulos(string s_tipoGuargar, string est002) //metodo para insertar enc y ren egresos
        {
            lblError.Text = "";
            string codemp = Session["gs_CodEmp"].ToString();
            string numfac = Session["gs_Numfac"].ToString();
            string usuing = Session["gs_CodUs1"].ToString();

            string s_enc = "";

            if (s_enc.Equals("")) //si no hubo error al insertar en el encabezado
            {
                DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];
                int rowIndex = 0; //para enconctrar ddl en grvObservacion                
                foreach (DataRow row in dtArticulos.Rows)
                {
                    Session.Add("gs_Codart", row.Field<string>("codart"));
                    string codart1 = row.Field<string>("codart");
                    //string nomart1 = row.Field<string>("nomart");
                    string observ1 = "";
                    string estdes1 = "";
                    if (!DBNull.Value.Equals(row["estdes"])) //para saber si estdes es NULL
                    {
                        estdes1 = row.Field<string>("estdes");
                    }

                    decimal cantidadDes = row.Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                    decimal saldo = row.Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                    //decimal cantid1 = saldo - cantidadDes; //articulos despachados
                    decimal cantid1 = cantidadDes; //articulos despachados

                    decimal candes1 = 0;
                    if (!DBNull.Value.Equals(row["candes"]))
                    {
                        candes1 = row.Field<decimal>("candes");//cantidad guardada en campo candes al hacer grabar pendiente
                    }

                    //***para obtener observ 
                    CheckBox rBoton = (CheckBox)grvRenglonesImp.Rows[rowIndex].Cells[5].FindControl("chkConfirmado");
                    if (rBoton.Checked) //si se hizo clic en btnAceptar
                    {
                        DropDownList ddlgrvObserv = (DropDownList)grvRenglonesImp.Rows[rowIndex].Cells[5].FindControl("ddlgrvObservRen");
                        observ1 = ddlgrvObserv.SelectedValue.ToString().Trim();
                    }


                    if (cantid1 != 0) //si no toco al articulo, no lo inserta en la tabla (si su cantid1 es 0)
                    {
                        //if (candes1 != 0) //si hace despacho parcial en un articulo que estaba con grabar pendiente
                        //{
                        //    cantid1 = cantid1 + candes1;
                        //}
                        if (lblError.Text.Trim() == "") //control de errores
                        {
                            f_llenarRenglonesIngresos(codart1, codemp, numfac, cantid1, cantidadDes);
                        }
                    }
                    else
                    {
                        if (estdes1.Equals("1") && candes1 != 0) // si fue grabado pendiente
                        {
                            if (lblError.Text.Trim() == "") //control de errores
                            {
                                f_llenarRenglonesIngresos(codart1, codemp, numfac, candes1, cantidadDes);
                            }
                        }
                        //else //actualizar la observacion
                        //{
                        //    if (lblError.Text.Trim() == "" && !(observ1.Equals(""))) //control de errores
                        //    {
                        //        f_actualizarObsev(codart1, codemp, numfac, observ1); //para actualizar las observ
                        //    }
                        //}
                    }
                    rowIndex++;

                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        f_actualizarObsev(codart1, codemp, numfac, observ1); //para actualizar las observ
                    }
                }
            }
        }

        public void f_llenarRenglonesIngresos(string codart1, string codemp, string numfac, decimal cantid1,
            decimal canfac)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_IMPORT_REN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@cantid", cantid1);
            cmd.Parameters.AddWithValue("@canfac", canfac);
            //cmd.Parameters.AddWithValue("@observ", observ);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*04. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }

        public void f_actualizarObsev(string codart1, string codemp, string numfac, string observ)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_IMPORT_REN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@observ", observ);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*14. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }
        public clsArticulo f_BuscarArticulo(string s_codart)
        {
            clsArticulo objArticulo = new clsArticulo();
            DataTable dt = (DataTable)Session["gdt_Articulos"];

            try
            {
                //int rowIndex = 0; //para enconctrar ddl en grvObservacion                
                foreach (DataRow row in dt.Rows)
                {
                    string s_desart = "ZZZ"; //no poner vacio, ZZZ para que el al buscar nada en txtBuscar no haya problemas
                    if (!DBNull.Value.Equals(row["desart"])) //para saber si desart es NULL
                    {
                        s_desart = row.Field<string>("desart").Trim();
                    }

                    if (row.Field<string>("codart").Trim().Equals(s_codart.Trim()) || s_desart.Equals(s_codart.Trim()))
                    {
                        if ((int)row.Field<decimal>("cansol") != (int)row.Field<decimal>("cantid"))
                        {
                            //objArticulo.Error = "AVISO: Cantidad ingresada es superior a Solicitada.";
                            objArticulo.Error = "CORRECTO";
                        }
                        else
                        {
                            objArticulo.Error = "CORRECTO";
                        }
                        objArticulo.Codart = s_codart;
                        string s_nomart = row.Field<string>("nomart");
                        objArticulo.Nomart = s_nomart;
                        int i_cantid = (int)row.Field<decimal>("cantid") + 1;

                        //actualiza el dt
                        row["cantid"] = i_cantid;

                        objArticulo.Cantid = (int)row.Field<decimal>("cansol") - (int)row.Field<decimal>("cantid");
                        //objArticulo.Error = "CORRECTO";

                        Session["gdt_Articulos"] = dt;
                        grvRenglonesImp.Columns[6].Visible = true; //para q f_MostrarCodAlterno() funcione
                        grvRenglonesImp.DataSource = dt;
                        grvRenglonesImp.DataBind();

                        f_pintarGridview(s_codart, i_cantid);
                        f_MostrarCodAlterno(); //PARA MOSTRAR CODIGO ALTERNO

                        grvRenglonesImp.Columns[6].Visible = false; //ocultar columna extra...DEBE IR AQUI PARA QUE COLORES DE GRV FUNCIONEN
                        Session["gs_ArtDespachado"] = "1"; //para saber que ha despachado por lo menos 1 articulo..se usa en f_insertarArticulos
                        return objArticulo;

                        //else
                        //{
                        //    objArticulo.Codart = "-";
                        //    objArticulo.Nomart = "-";
                        //    objArticulo.Cantid = 0;
                        //    objArticulo.Error = "*15. Cantidad ingresada es superior a Solicitada. Despacho no realizado.";
                        //    return objArticulo;
                        //}

                    }
                    //if (!DBNull.Value.Equals(dt.Rows[rowIndex]["observ"])) //si observ no es NULL
                    //{
                    //    CheckBox rBoton = (CheckBox)grvRenglonesImp.Rows[rowIndex].Cells[5].FindControl("chkConfirmado");
                    //    rBoton.Checked = true;
                    //    DropDownList ddlgrvObserv = (DropDownList)grvRenglonesImp.Rows[rowIndex].Cells[5].FindControl("ddlgrvObservRen");
                    //    ddlgrvObserv.SelectedValue = dt.Rows[rowIndex]["observ"].ToString().Trim();
                    //    ddlgrvObserv.DataBind();
                    //}
                    //rowIndex++;
                }
            }
            catch (Exception ex)
            {
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "*02. " + DateTime.Now + " " + ex.Message;
                return objArticulo;
            }

            objArticulo.Codart = "-";
            objArticulo.Nomart = "-";
            objArticulo.Cantid = 0;
            objArticulo.Error = "*03. " + DateTime.Now + " " + " No se encontró artículo.";
            return objArticulo;
        }

        public void f_pintarGridview(string s_codart, int i_cantid) //pinta y llena el chkConfirmar
        {
            foreach (GridViewRow row in grvRenglonesImp.Rows)
            {

                //if ((int)decimal.Parse(row.Cells[2].Text.Trim()) == 0) //llena el chkConfirmar
                //{
                //    CheckBox rBoton = (CheckBox)row.Cells[4].FindControl("chkConfirmado");
                //    rBoton.Checked = true;
                //}
                string codart1 = row.Cells[1].Text.Trim();
                string alterno = row.Cells[6].Text.Trim();

                if (row.Cells[1].Text.Trim().Equals(s_codart.Trim()) || row.Cells[6].Text.Trim().Equals(s_codart.Trim())) //pinta la fila con codart
                {
                    if (i_cantid.ToString().Trim() == row.Cells[2].Text.Trim())
                        row.BackColor = Color.LightGreen;
                    else
                    {
                        if (i_cantid > int.Parse(row.Cells[2].Text.Trim())) //uuuu
                        {
                            row.BackColor = Color.LightCoral;
                        }
                        else
                        {
                            row.BackColor = Color.Khaki;
                        }
                    }                        
                }
            }
        }


        protected void btnDespachar_Click(object sender, EventArgs e)
        {
            clsArticulo objArticulo = new clsArticulo();
            try
            {
                objArticulo.Codart = txtCodart.Text;
                objArticulo.Nomart = txtNomart.Text;
                if (String.IsNullOrEmpty(txtSaldo.Text))
                    objArticulo.Cantid = 0;
                else
                    objArticulo.Cantid = (int)decimal.Parse(txtSaldo.Text);
                objArticulo.Error = "";
                DataTable dt = (DataTable)Session["gdt_Articulos"];
                string s_codart = txtCodart.Text.Trim();

                int i_cantidDesapachar = 1;
                if (!txtCantid.Text.Equals("")) //si deja vacio txtCantid se asume que es 1
                {
                    i_cantidDesapachar = int.Parse(txtCantid.Text.ToString());
                }

                foreach (DataRow row in dt.Rows)
                {
                    string s_desart = "ZZZ"; //no poner vacio, ZZZ para que el al buscar nada en txtBuscar no haya problemas
                    if (!DBNull.Value.Equals(row["desart"])) //para saber si desart es NULL
                    {
                        s_desart = row.Field<string>("desart").Trim();
                    }

                    if (row.Field<string>("codart").Trim().Equals(s_codart.Trim()) || s_desart.Equals(s_codart.Trim()))
                    {
                        //if ((int)decimal.Parse(txtSaldo.Text) < i_cantidDesapachar) //si ingresa cantidad mayor que saldo existente
                        //{
                        //    txtCantid.Text = "1";
                        //    objArticulo.Error = "*10. Cantidad ingresada es superior a Saldo. Despacho no realizado.";
                        //    f_ActualizarCampos(objArticulo);
                        //    objArticulo.Error = "AVISO: Cantidad ingresada es superior a Solicitada.";
                        //    //objArticulo.Error = "CORRECTO";
                        //}
                        //else
                        //{
                        //    objArticulo.Error = "CORRECTO";
                        //}
                        objArticulo.Error = "CORRECTO";
                        objArticulo.Codart = s_codart;
                        string s_nomart = row.Field<string>("nomart");
                        objArticulo.Nomart = s_nomart;
                        int i_cantid = (int)row.Field<decimal>("cantid") + i_cantidDesapachar;

                        //actualiza el dt
                        row["cantid"] = i_cantid;

                        objArticulo.Cantid = (int)row.Field<decimal>("cansol") - (int)row.Field<decimal>("cantid"); ;
                        //objArticulo.Error = "CORRECTO";

                        Session["gdt_Articulos"] = dt;
                        grvRenglonesImp.Columns[6].Visible = true; //para q f_MostrarCodAlterno() funcione
                        grvRenglonesImp.DataSource = dt;
                        grvRenglonesImp.DataBind();

                        f_pintarGridview(s_codart, i_cantid);
                        f_MostrarCodAlterno(); //PARA MOSTRAR CODIGO ALTERNO

                        grvRenglonesImp.Columns[6].Visible = false; //ocultar columna extra...DEBE IR AQUI PARA QUE COLORES DE GRV FUNCIONEN
                        f_ActualizarCampos(objArticulo);
                    }
                }
                //aqui poner objArticulo.Codart = "-"; .....
            }
            catch (Exception ex)
            {
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "*11. " + DateTime.Now + " " + ex.Message;
                f_ActualizarCampos(objArticulo);
            }
        }

        public void f_ActualizarCampos(clsArticulo objArticulo)
        {
            txtCodart.Text = objArticulo.Codart;
            txtNomart.Text = objArticulo.Nomart;
            txtSaldo.Text = objArticulo.Cantid.ToString();
            if (!objArticulo.Error.Equals("CORRECTO"))
            {
                lblError.Text = "*01. " + DateTime.Now + objArticulo.Error;
            }
            else
            {
                lblError.Text = "";
            }
            txtBuscar.Text = "";
            int rowIndex = 0;
            DataTable dt = (DataTable)Session["gdt_Articulos"];
            foreach (GridViewRow row in grvRenglonesImp.Rows)
            {                
                try
                {
                    CheckBox rBoton = (CheckBox)row.Cells[5].FindControl("chkConfirmado");
                    if (!DBNull.Value.Equals(dt.Rows[rowIndex]["observ"])) //si observ no es NULL
                    {
                        rBoton.Checked = true;
                        DropDownList ddlgrvObserv = (DropDownList)row.Cells[5].FindControl("ddlgrvObservRen");
                        ddlgrvObserv.SelectedValue = dt.Rows[rowIndex]["observ"].ToString().Trim();
                        ddlgrvObserv.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    lblError.Text = "*21. " + DateTime.Now + " " + ex.Message;
                }
                rowIndex++;
            }
        }

        protected void btnPendiente_Click(object sender, EventArgs e) //para que despachen cierta cantidad y se guarde al volver a ingresar (NO INSERTA EN TBLS...solo hace update)
        {
            string codemp = Session["gs_CodEmp"].ToString();
            string numfac = Session["gs_Numfac"].ToString();

            //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (AZUL)
            if (lblError.Text.Trim() == "") //control de errores
            {
                f_actualizarColoresGrv(codemp, numfac,
                    "1", "3", Session["gs_CodUs1"].ToString());
            }

            DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];
            int rowIndex = 0; //para enconctrar ddl en grvObservacion       
            foreach (DataRow row in dtArticulos.Rows)
            {
                Session.Add("gs_Codart", row.Field<string>("codart"));
                string codart1 = row.Field<string>("codart");
                //string nomart1 = row.Field<string>("nomart");
                //string coduni1 = row.Field<string>("coduni");
                //decimal preuni1 = row.Field<decimal>("preuni");  
                string observ1 = "";
                decimal cantidadDes = row.Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                decimal saldo = row.Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                //decimal cantid1 = saldo + cantidadDes; //articulos despachados
                decimal cantid1 = cantidadDes; //articulos despachados

                //NUEVO CODIGO 20200217 PARA QUE AL GRABAR PENDIENTE 2 VECES EL MISMO ARTICULO SUME LAS CANDES
                decimal candes1 = 0;
                if (!DBNull.Value.Equals(row["candes"]))
                {
                    candes1 = row.Field<decimal>("candes");//cantidad guardada en campo candes al hacer grabar pendiente
                }

                //***para obtener observ 
                CheckBox rBoton = (CheckBox)grvRenglonesImp.Rows[rowIndex].Cells[5].FindControl("chkConfirmado");
                if (rBoton.Checked) //si se hizo clic en btnAceptar
                {
                    DropDownList ddlgrvObserv = (DropDownList)grvRenglonesImp.Rows[rowIndex].Cells[5].FindControl("ddlgrvObservRen");
                    observ1 = ddlgrvObserv.SelectedValue.ToString().Trim();
                }

                if (cantid1 != 0 || rBoton.Checked) //si no toco al articulo, no le modifica su estdes ni su canfac
                {
                    //if (candes1 != 0) //si hace grabar pendiente en un articulo que estaba con grabar pendiente
                    //{
                    //    cantid1 = cantid1 + candes1;
                    //}

                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        f_actualizarRenglonesIngPar(codart1, codemp, numfac, cantidadDes, cantid1, observ1);
                    }
                }
                rowIndex++;
            }

            if (lblError.Text.Trim() == "") //control de errores
            {
                Response.Redirect("w_ImportListado.aspx"); //ojo manejar error
            }
        }

        public void f_actualizarColoresGrv(string codemp, string numfac, string est001,
           string est002, string usu001)
        {
            //para actualizar los colores del enbabezadopedpro
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_COLOR_IMPORT_ENC]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@est001", est001);
            cmd.Parameters.AddWithValue("@est002", est002);
            cmd.Parameters.AddWithValue("@usu001", usu001);
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*07. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }

        public void f_actualizarRenglonesIngPar(string codart1, string codemp, string numfac,
            decimal canfac, decimal candes, string observ)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_GRABAR_PENDIENTE_IMP]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@canfac", canfac);
            cmd.Parameters.AddWithValue("@candes", candes);
            cmd.Parameters.AddWithValue("@observ", observ);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*13. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }
    }
}