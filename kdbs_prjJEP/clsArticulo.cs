﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kdbs_prjJEP
{
    public class clsArticulo
    {
        public string Codart { get; set; }
        public string Nomart { get; set; }
        public int Cantid { get; set; } //representa el saldo existente (txtSaldo)
        public decimal Preuni { get; set; }
        public string Coduni { get; set; }
        public string Error { get; set; }
    }
}