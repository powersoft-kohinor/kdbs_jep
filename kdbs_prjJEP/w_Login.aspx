﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="w_Login.aspx.cs" Inherits="kdbs_prjJEP.w_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">


  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>

  <title>Kohinor Web - JEP</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"/>
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"/>

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet"/>

</head>

 <body class="bg-gradient-primary">
<form id="form1" runat="server" defaultbutton="btnLogin">
  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block">
                  <asp:Image ID="Image1" runat="server" ImageUrl="~/Imagenes/JEP2.jpeg" Height="100%" Width="100%" />

              </div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Imagenes/Logo-Kohinor2.png" Width="100px" />

                  </div>


                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Kohinor Web</h1>
                      <%--<h1 class="h4 text-gray-900 mb-4">¡Bienvenido!</h1>--%>
                  </div>
                  <hr>
                  <form class="user">
                    <div class="form-group">
                      <asp:DropDownList ID="ddlEmpresa" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="txtUsuario" aria-describedby="emailHelp" placeholder="Ingrese Usuario..." runat="server">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="txtPassword" placeholder="Password" runat="server">
                    </div>
                    <%--<div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div>
                    </div>--%>
                    <div class="form-group">
                        <asp:Label ID="lblError" runat="server" ForeColor="#FF3300"></asp:Label>
                    </div>
                    <asp:Button ID="btnLogin" runat="server" Text="Login" class="btn btn-primary btn-user btn-block" OnClick="btnLogin_Click"/>
                    <%--<a href="w_Inicio.aspx" class="btn btn-primary btn-user btn-block">
                      Login
                    </a>--%>
                    
                   
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="">¿Olvidó la contraseña?</a>
                      <%--<asp:HyperLink ID="hpkForgotPass" class="small" runat="server" NavigateUrl="forgot-password.html">Forgot Password?</asp:HyperLink>--%>
                  </div>
                  <div class="text-center">
                    <a class="small" href="">Crear un cuenta!</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
         
       
    </form>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
     
  <!--i SQL DATASOURCES-->
  <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:kdbs_EsperanzaConnectionString %>" SelectCommand="W_CRM_S_DDLEMPRESA" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
  <!--f SQL DATASOURCES-->

</body>

</html>

