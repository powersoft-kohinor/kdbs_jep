﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


//LAST ERROR CODE: *03.

namespace kdbs_prjJEP
{
    public partial class w_TomaFisica1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["gs_CodUs1"] != null)
                {
                    lblUsuario.Text = Session["gs_CodUs1"].ToString();
                    lblUsuarioDrop.Text = Session["gs_CodUs1"].ToString();
                }

                Session.Add("gs_ContVal", "0"); //para que al aplastar muchas veces solo se haga 1
                txtFecegr.Text = DateTime.Now.ToString("yyyy-MM-dd");
                ////SECUENCIA N° EGR
                //txtNumegr.Text = f_CalcularSecuencia();
                txtNumegr.Text = Session["gs_Numfac"].ToString();
                //txtTittar.Text = Session["gs_Tittar"].ToString();
                txtNumtar.Text = Session["gs_Numtar"].ToString();

                //PARA LLENAR grvRenglonespedpro
                DataSourceSelectArguments args = new DataSourceSelectArguments(); //para pasar del SqlDataSource1 a una DataTable
                DataView view = (DataView)sqldsRenglonesIng.Select(args);            //para pasar del SqlDataSource1 a una DataTable
                DataTable dt = view.ToTable(); //en esta dataTable esta el pedido seleccionado... (con los datos de este se llenara el encabezadoegresos)
                //para agregar saldo (cantid original)
                dt.Columns.Add("saldo", typeof(decimal));
                foreach (DataRow row in dt.Rows)
                {
                    row["saldo"] = row.Field<decimal>("cantid"); //guarda el total de cada articulo en columna saldo

                    if (!DBNull.Value.Equals(row["canfac"])) //para saber si canfac es NULL
                    {
                        row["cantid"] = row.Field<decimal>("canfac"); //si han hecho despacho parcial el valor de canfac debe guardarse en cantid
                        row["saldo"] = row["cantid"];
                    }
                }

                Session.Add("gdt_Articulos", dt);
                grvRenglonesIng.DataSource = dt;
                grvRenglonesIng.DataBind();

                //la cantid puede ser 0 cuando hayan realizado un despacho parcial
                //para esto se llena el chk y se pinta de verde apenaz ingrese a Despacho.aspx
                int rowIndex = 0;
                foreach (GridViewRow row in grvRenglonesIng.Rows)
                {
                    if ((int)decimal.Parse(row.Cells[2].Text.Trim()) > 0) //llena el chkConfirmar
                    {
                        //CheckBox rBoton = (CheckBox)row.Cells[4].FindControl("chkConfirmado");
                        //rBoton.Checked = true;
                        row.BackColor = Color.LightGreen;
                    }

                    //para poner color azul en caso de que haya sido grabado pendiente
                    string estdes = "";
                    if (!DBNull.Value.Equals(dt.Rows[rowIndex]["estdes"])) //si estdes no es NULL
                    {
                        estdes = dt.Rows[rowIndex].Field<string>("estdes");
                    }

                    if (estdes.Equals("1"))
                    {
                        row.BackColor = Color.DodgerBlue;
                    }

                    //PARA MOSTRAR EL CODIGO ALTERNO EN LUGAR DE CODART....SOLO SI NO ES NULL
                    //if (row.Cells[5].Text != "&nbsp;") //para saber si desart es NULL
                    //{
                    //    string s_aux = row.Cells[1].Text; //PARA PASAR desart A Cells[1] y codart  A Cells[5]
                    //    row.Cells[1].Text = row.Cells[5].Text; //Muestra desart en Código
                    //    row.Cells[5].Text = s_aux;
                    //}
                    rowIndex++;
                }
                //grvRenglonespedpro.Columns[5].Visible = false; //esconde la columna de desart
                
                
            }

            if (IsPostBack)
            {
                if (!txtBuscar.Text.Equals(""))
                {
                    string codart = txtBuscar.Text.Trim();
                    Session.Add("gs_Codart", codart);
                    if (!codart.Equals(""))
                    {
                        clsArticulo objArticulo = f_BuscarArticulo(codart);
                        f_ActualizarCampos(objArticulo);
                        txtBuscar.Focus();
                    }
                }
            }
        }

        protected void lnbtnListadoDespacho_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_Inicio.aspx");
        }

        protected void lnbtnDespacho_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Despacho primero.');</script>");
        }

        protected void lnbtnSalida_Click(object sender, EventArgs e)
        {
            //Response.Redirect("w_Salida.aspx");
            Response.Redirect("w_Login.aspx");
        }

        protected void lnbtnListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TomaFisicaListado.aspx");
        }
        protected void lnbtnTomaFisica_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TomaFisica.aspx");
        }

        protected void lnbtnImportListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_ImportListado.aspx");
        }

        protected void lnbtnImportaciones_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Importaciones primero.');</script>");
        }

        protected void lnbtnTransferListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TransferListado.aspx");
        }

        protected void lnbtnTransferencias_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Transferencias primero.');</script>");
        }

        //***i CALCULO SECUENCIAS**********************************************************
        //public string f_CalcularSecuencia()
        //{
        //    //ANTES DE LLENAR ENCABEZADO EGRESOS SE CALCULA EL NUMFAC
        //    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        //    conn.Open();

        //    SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_SECUENCIA]", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@codemp", Session["gs_CodEmp"].ToString());
        //    cmd.Parameters.AddWithValue("@codsec", "CP_ING");
        //    cmd.Parameters.AddWithValue("@sersec", Session["gs_SerSec"].ToString()); //del usuario en Login

        //    cmd.Parameters.Add("@sp_NumCom", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;

        //    cmd.ExecuteNonQuery();
        //    string s_numfac = cmd.Parameters["@sp_NumCom"].Value.ToString();

        //    conn.Close();

        //    Session.Add("gs_NumFac", s_numfac);
        //    return s_numfac;
        //}

        //public void f_ActualizarSecuencia()
        //{
        //    //ACTUALIZA LA SECUENCIA CUANDO EL USUARIO SELECCIONA GUARDAR.
        //    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        //    conn.Open();

        //    SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_SEC2]", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@codemp", Session["gs_CodEmp"].ToString());
        //    cmd.Parameters.AddWithValue("@codsec", "CP_ING");
        //    cmd.Parameters.AddWithValue("@sersec", Session["gs_SerSec"].ToString()); //del usuario en Login

        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //    }
        //    catch (Exception ex)
        //    {
        //        lblError.Text = "*12. " + DateTime.Now + " " + ex.Message;
        //        //throw ex;
        //    }
        //    conn.Close();
        //}

        //***f CALCULO SECUENCIAS**********************************************************


        //***i LLENAR GRVRENGLONESPEDPRO**********************************************************
        //public void f_llenarGrvRenglonespedpro()
        //{
        //    clsArticulo objArticulo = new clsArticulo();
        //    DataTable dt = new DataTable();
        //    DataRow dr;
        //    dt.Columns.Add("sno");
        //    dt.Columns.Add("codart");
        //    dt.Columns.Add("cantid");
        //    dt.Columns.Add("nomart");
        //    dt.Columns.Add("coduni");
        //    dt.Columns.Add("preuni");

        //    if (Session["buyitems"] == null)
        //    {
        //        dr = dt.NewRow();
        //        objArticulo = f_llenarDtArticulos(dt, dr);
        //        f_ActualizarCampos(objArticulo);
        //    }
        //    else
        //    {
        //        dt = (DataTable)Session["buyitems"];                
        //        int sr = dt.Rows.Count; //para saber el total de filas en el dt..no se esta usando
        //        dr = dt.NewRow();
        //        objArticulo = f_llenarDtArticulos(dt, dr);
        //        f_ActualizarCampos(objArticulo);
        //    }
        //}

        //public clsArticulo f_llenarDtArticulos(DataTable dt, DataRow dr) //para llenar Session buyitems al pitar un articulo
        //{
        //    clsArticulo objArticulo = new clsArticulo();
        //    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        //    conn.Open();

        //    SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_S_PEDPRO_REN]", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@CODART", Session["gs_Codart"].ToString());
        //    cmd.Parameters.AddWithValue("@codus1", Session["gs_CodUs1"].ToString());
        //    cmd.ExecuteNonQuery();
        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataSet ds = new DataSet();
        //    da.Fill(ds);

        //    conn.Close();
        //    try
        //    {
        //        string s_codart = ds.Tables[0].Rows[0]["codart"].ToString();
        //        string s_nomart = ds.Tables[0].Rows[0]["nomart"].ToString();
        //        string s_coduni = ds.Tables[0].Rows[0]["coduni"].ToString();
        //        decimal s_preuni = decimal.Parse(ds.Tables[0].Rows[0]["preuni"].ToString());
        //        int i_cantid = 1;

        //        objArticulo.Codart = s_codart;
        //        objArticulo.Nomart = s_nomart;
        //        objArticulo.Cantid = i_cantid;
        //        objArticulo.Error = "CORRECTO";

        //        dr["sno"] = 1;
        //        dr["codart"] = s_codart;
        //        dr["cantid"] = i_cantid;
        //        dr["nomart"] = s_nomart;
        //        dr["coduni"] = s_coduni;
        //        dr["preuni"] = s_preuni;

        //        dt.Rows.Add(dr);
        //        grvRenglonespedpro.DataSource = dt;
        //        grvRenglonespedpro.DataBind();

        //        Session["buyitems"] = dt;
        //        //grvRenglonespedpro.FooterRow.Cells[5].Text = "Cantidad Total";
        //        return objArticulo;
        //    }
        //    catch (Exception ex)
        //    {
        //        objArticulo.Codart = "-";
        //        objArticulo.Nomart = "-";
        //        objArticulo.Cantid = 0;
        //        objArticulo.Error = "*02. " + DateTime.Now + "Artículo no registrado.";
        //        return objArticulo;
        //    }
        //}

        //***i LLENAR GRVRENGLONESPEDPRO**********************************************************

        public int f_CanTot() //PARA CALCULAR CANTID TOTAL DE grvRenglonespedpro...(PARA VALIDACION DE btnGuardar)
        {
            //1 -->PARCIAL (ALGUNO TIENE CANTID 0)
            //0 -->COMPLETO(NINGUNO ESTA CON CANTID 0)
            int i_cantot = 0;
            foreach (GridViewRow row in grvRenglonesIng.Rows)
            {
                if (decimal.Parse(row.Cells[2].Text.Trim())==0)
                {
                    i_cantot = 1;
                }
                //i_cantot = i_cantot + (int)decimal.Parse(row.Cells[2].Text.Trim());
            }
            return i_cantot;
        }


        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (f_CanTot() == 0) //verifica si todos los cantid estan en 0....REALIZO PARCIAL PERO TODOS ESTABAN EN CERO....SE PONE EN VERDE
            {
                if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
                {
                    Session["gs_ContVal"] = "1";
                    f_insertarArticulos("btnGuardar", "1"); //est002=1 ...para q wladi sepa si fue parcial o completo

                    //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (VERDE)
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numfac"].ToString(),
                            "1", "1", Session["gs_CodUs1"].ToString());
                    }

                    //redireccion despues de guardar
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        Response.Redirect("w_TomaFisicaListado.aspx");
                    }
                }
            }
            else //GRABADO PARCIAL NORMAL
            {
                btnGuardarParcial_Click();
            }
        }

        protected void btnAceptar_Click(object sender, EventArgs e) //para guardarParcial
        {
            if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
            {
                Session["gs_ContVal"] = "1";
                f_insertarArticulos("btnAceptar", "2"); //est002=2 ...para q wladi sepa si fue parcial o completo

                //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (LILA)
                if (lblError.Text.Trim() == "") //control de errores
                {
                    f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numfac"].ToString(),
                        "1", "2", Session["gs_CodUs1"].ToString());
                }

                //redireccion despues de guardar
                if (lblError.Text.Trim() == "") //control de errores
                {
                    Response.Redirect("w_TomaFisicaListado.aspx");
                }
            }            
        }

        protected void btnGuardarParcial_Click()
        {
            lblAlertaModal.Text = "";
            int rowIndex = 0;
            DataTable dtObservacionArticulos = (DataTable)Session["gdt_Articulos"];

            //PARA MOSTRAR ROW DE LOS ARTICULOS QUE NO SE VEN A INSERTAR (SU CANTID NO ES CERO)
            //for (int i = 0; i < dtObservacionArticulos.Rows.Count; i++)
            //{

            //    if (cantidadDes == 0) //si sobran articulos los muestra (si su cantidadDes es 0)
            //    {
            //        dtObservacionArticulos.Rows.RemoveAt(i);
            //    }
            //}

            grvObservacionArticulos.DataSource = dtObservacionArticulos;
            grvObservacionArticulos.DataBind();

            foreach (GridViewRow row in grvObservacionArticulos.Rows)
            {

                //PARA MOSTRAR ROW DE LOS ARTICULOS QUE NO SE VEN A INSERTAR (SU CANTID ES CERO)
                decimal cantidadDes = dtObservacionArticulos.Rows[rowIndex].Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                //decimal saldo = dt.Rows[rowIndex].Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                //decimal cantid1 = saldo - cantidadDes; //articulos despachados

                if (cantidadDes == 0) // muestra (si su cantidadDes  es 0)
                {
                    grvObservacionArticulos.Rows[rowIndex].Visible = true;
                }
                else
                {
                    grvObservacionArticulos.Rows[rowIndex].Visible = false;
                }
                rowIndex++;
            }

            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "$('#modalObservacion').modal('show')", true);
            //de aqui va al btnAceptar
        }

        public void f_insertarArticulos(string s_tipoGuargar, string est002) //metodo para insertar enc y ren egresos
        {
            lblError.Text = "";
            string codemp = Session["gs_CodEmp"].ToString();
            string numfac = Session["gs_Numfac"].ToString();
            string usuing = Session["gs_CodUs1"].ToString();

            string s_enc = "";

            if (s_enc.Equals("")) //si no hubo error al insertar en el encabezado
            {
                DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];
                int rowIndex = 0; //para enconctrar ddl en grvObservacion                
                foreach (DataRow row in dtArticulos.Rows)
                {
                    Session.Add("gs_Codart", row.Field<string>("codart"));
                    string codart1 = row.Field<string>("codart");
                    //string nomart1 = row.Field<string>("nomart");
                    string observ1 = "";
                    string estdes1 = "";
                    if (!DBNull.Value.Equals(row["estdes"])) //para saber si estdes es NULL
                    {
                        estdes1 = row.Field<string>("estdes");
                    }

                    decimal cantidadDes = row.Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                    decimal saldo = row.Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                    //decimal cantid1 = saldo - cantidadDes; //articulos despachados
                    decimal cantid1 = cantidadDes; //articulos despachados

                    decimal candes1 = 0;
                    if (!DBNull.Value.Equals(row["candes"]))
                    {
                        candes1 = row.Field<decimal>("candes");//cantidad guardada en campo candes al hacer grabar pendiente
                    }

                    //***para obtener observ de grvObservacion al hacer clic en btnAceptar
                    if (s_tipoGuargar.Equals("btnAceptar")) //si se hizo clic en btnAceptar
                    {
                        DropDownList ddlgrvObserv = (DropDownList)grvObservacionArticulos.Rows[rowIndex].Cells[3].FindControl("ddlgrvObservacion");
                        if (cantidadDes == 0) //muestra (si su cantidadDes  es 0)
                        {
                            observ1 = ddlgrvObserv.SelectedValue.ToString().Trim();
                        }
                    }
                        

                    if (cantid1 != 0) //si no toco al articulo, no lo inserta en la tabla (si su cantid1 es 0)
                    {
                        //if (candes1 != 0) //si hace despacho parcial en un articulo que estaba con grabar pendiente
                        //{
                        //    cantid1 = cantid1 + candes1;
                        //}
                        if (lblError.Text.Trim() == "") //control de errores
                        {
                            f_llenarRenglonesIngresos(codart1, codemp, numfac, cantid1, cantidadDes, observ1);
                        }
                    }
                    else
                    {
                        if (estdes1.Equals("1") && candes1 != 0) // si fue grabado pendiente
                        {
                            if (lblError.Text.Trim() == "") //control de errores
                            {
                                f_llenarRenglonesIngresos(codart1, codemp, numfac, candes1, cantidadDes, observ1);
                            }
                        }
                        else //actualizar la observacion
                        {
                            if (lblError.Text.Trim() == "" && !(observ1.Equals(""))) //control de errores
                            {
                                f_actualizarObsev(codart1, codemp, numfac, observ1); //para actualizar las observ
                            }
                        }
                    }
                    rowIndex++;

                    //if (lblError.Text.Trim() == "" && !(observ1.Equals(""))) //control de errores
                    //{
                    //    f_actualizarObsev(codart1, codemp, numtra, observ1); //para actualizar las observ
                    //}
                }
            }
        }

        //public string f_llenarEncabezadoIngresos(string codemp, string numfac, string usuing, string est002)
        //{
        //    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        //    conn.Open();

        //    SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_EGRESOS_ENC]", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@codemp", codemp);
        //    cmd.Parameters.AddWithValue("@numfac", numfac);
        //    cmd.Parameters.AddWithValue("@usuing", usuing);
        //    cmd.Parameters.AddWithValue("@est002", est002);

        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //        conn.Close();
        //        return ""; //se inserto correctamente
        //    }
        //    catch (Exception ex)
        //    {
        //        conn.Close();
        //        lblError.Text = "*03. " + DateTime.Now + " " + ex.Message;
        //        return "*03. " + DateTime.Now + " " + ex.Message;
        //        //throw ex;
        //    }
        //}

        public void f_llenarRenglonesIngresos(string codart1, string codemp, string numfac, decimal cantid1,
            decimal canfac, string observ)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_INGRESOS_REN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@cantid", cantid1);
            cmd.Parameters.AddWithValue("@canfac", canfac);
            cmd.Parameters.AddWithValue("@observ", observ);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*04. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }

        public void f_actualizarObsev(string codart1, string codemp, string numfac, string observ)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_INGRESOS_REN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@observ", observ);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*14. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }
        public clsArticulo f_BuscarArticulo(string s_codart)
        {
            clsArticulo objArticulo = new clsArticulo();
            DataTable dt = (DataTable)Session["gdt_Articulos"];

            try
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row.Field<string>("codart").Trim().Equals(s_codart.Trim()))
                    {
                        objArticulo.Codart = s_codart;
                        string s_nomart = row.Field<string>("nomart");
                        objArticulo.Nomart = s_nomart;
                        int i_cantid = (int)row.Field<decimal>("cantid") + 1;

                        //actualiza el dt
                        row["cantid"] = i_cantid;

                        objArticulo.Cantid = i_cantid;
                        objArticulo.Error = "CORRECTO";

                        Session["gdt_Articulos"] = dt;
                        //grvRenglonespedpro.Columns[5].Visible = true; //para q f_MostrarCodAlterno() funcione
                        grvRenglonesIng.DataSource = dt;
                        grvRenglonesIng.DataBind();

                        //f_MostrarCodAlterno(); //PARA MOSTRAR CODIGO ALTERNO
                        f_pintarGridview(s_codart, i_cantid);
                        //grvRenglonespedpro.Columns[5].Visible = false; //ocultar columna extra...DEBE IR AQUI PARA QUE COLORES DE GRV FUNCIONEN
                        Session["gs_ArtDespachado"] = "1"; //para saber que ha despachado por lo menos 1 articulo..se usa en f_insertarArticulos
                        return objArticulo;
                    }
                }
            }
            catch (Exception ex)
            {
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "*02. " + DateTime.Now + " " + ex.Message;
                return objArticulo;
            }

            //if (s_codart.Any(char.IsDigit)) //si contiene por lo menos 1 numero (pistoleo el codigo....todo se hace automático)
            //{
                
            //}
            //else // si busco por nombre en txtBuscar
            //{
            //    Session.Add("gs_Nomart", s_codart.Trim());
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$(function() { $('#modalBuscar').modal('show'); });</script>", false);
            //    objArticulo.Codart = "-";
            //    objArticulo.Nomart = "-";
            //    objArticulo.Cantid = 0;
            //    objArticulo.Error = "";
            //    return objArticulo;
            //}

            objArticulo.Codart = "-";
            objArticulo.Nomart = "-";
            objArticulo.Cantid = 0;
            objArticulo.Error = "*03. " + DateTime.Now + " " + " No se encontró artículo.";
            return objArticulo;
        }

        public void f_pintarGridview(string s_codart, int i_cantid) //pinta y llena el chkConfirmar
        {
            foreach (GridViewRow row in grvRenglonesIng.Rows)
            {

                //if ((int)decimal.Parse(row.Cells[2].Text.Trim()) == 0) //llena el chkConfirmar
                //{
                //    CheckBox rBoton = (CheckBox)row.Cells[4].FindControl("chkConfirmado");
                //    rBoton.Checked = true;
                //}
                string codart1 = row.Cells[1].Text.Trim();
                //string alterno = row.Cells[5].Text.Trim();

                if (row.Cells[1].Text.Trim().Equals(s_codart.Trim())) //pinta la fila con codart
                {
                    if (i_cantid > 0)
                        row.BackColor = Color.LightGreen;
                    else
                        row.BackColor = Color.Khaki;
                }
            }
        }


        protected void btnDespachar_Click(object sender, EventArgs e)
        {
            clsArticulo objArticulo = new clsArticulo();
            try
            {
                objArticulo.Codart = txtCodart.Text;
                objArticulo.Nomart = txtNomart.Text;
                objArticulo.Cantid = (int)decimal.Parse(txtSaldo.Text);
                objArticulo.Error = "";
                DataTable dt = (DataTable)Session["gdt_Articulos"];
                string s_codart = txtCodart.Text.Trim();

                int i_cantidDesapachar = 1;
                if (!txtCantid.Text.Equals("")) //si deja vacio txtCantid se asume que es 1
                {
                    i_cantidDesapachar = int.Parse(txtCantid.Text.ToString());
                }

                foreach (DataRow row in dt.Rows)
                {
                    if (row.Field<string>("codart").Trim().Equals(s_codart.Trim()))
                    {
                        //if ((int)row.Field<decimal>("cantid") < i_cantidDesapachar) //si ingresa cantidad mayor que saldo existente
                        //{
                        //    txtCantid.Text = "1";
                        //    objArticulo.Error = "*10. Cantidad ingresada es superior a Saldo. Despacho no realizado.";
                        //    f_ActualizarCampos(objArticulo);
                        //}
                        //else //cantidad correcta
                        //{
                            
                        //}

                        objArticulo.Codart = s_codart;
                        string s_nomart = row.Field<string>("nomart");
                        objArticulo.Nomart = s_nomart;
                        int i_cantid = (int)row.Field<decimal>("cantid") + i_cantidDesapachar;

                        //actualiza el dt
                        row["cantid"] = i_cantid;

                        objArticulo.Cantid = i_cantid;
                        objArticulo.Error = "CORRECTO";

                        Session["gdt_Articulos"] = dt;
                        //grvRenglonespedpro.Columns[5].Visible = true; //para q f_MostrarCodAlterno() funcione
                        grvRenglonesIng.DataSource = dt;
                        grvRenglonesIng.DataBind();

                        //f_MostrarCodAlterno(); //PARA MOSTRAR CODIGO ALTERNO
                        //f_pintarGridview(s_codart, i_cantid, s_desart);
                        //grvRenglonespedpro.Columns[5].Visible = false; //ocultar columna extra...DEBE IR AQUI PARA QUE COLORES DE GRV FUNCIONEN
                        f_ActualizarCampos(objArticulo);
                    }
                }
                //aqui poner objArticulo.Codart = "-"; .....
            }
            catch (Exception ex)
            {
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "*11. " + DateTime.Now + " " + ex.Message;
                f_ActualizarCampos(objArticulo);
            }
        }

        public void f_ActualizarCampos(clsArticulo objArticulo)
        {
            txtCodart.Text = objArticulo.Codart;
            txtNomart.Text = objArticulo.Nomart;
            txtSaldo.Text = objArticulo.Cantid.ToString();
            if (!objArticulo.Error.Equals("CORRECTO"))
            {
                lblError.Text = "*01. " + DateTime.Now + objArticulo.Error;
            }
            else
            {
                lblError.Text = "";
            }
            txtBuscar.Text = "";
        }

        protected void btnPendiente_Click(object sender, EventArgs e) //para que despachen cierta cantidad y se guarde al volver a ingresar (NO INSERTA EN TBLS...solo hace update)
        {
            string codemp = Session["gs_CodEmp"].ToString();
            string numfac = Session["gs_Numfac"].ToString();

            //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (AZUL)
            if (lblError.Text.Trim() == "") //control de errores
            {
                f_actualizarColoresGrv(codemp, numfac,
                    "1", "3", Session["gs_CodUs1"].ToString());
            }

            DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];
            foreach (DataRow row in dtArticulos.Rows)
            {
                Session.Add("gs_Codart", row.Field<string>("codart"));
                string codart1 = row.Field<string>("codart");
                //string nomart1 = row.Field<string>("nomart");
                //string coduni1 = row.Field<string>("coduni");
                //decimal preuni1 = row.Field<decimal>("preuni");  

                decimal cantidadDes = row.Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                decimal saldo = row.Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                //decimal cantid1 = saldo + cantidadDes; //articulos despachados
                decimal cantid1 = cantidadDes; //articulos despachados

                //NUEVO CODIGO 20200217 PARA QUE AL GRABAR PENDIENTE 2 VECES EL MISMO ARTICULO SUME LAS CANDES
                decimal candes1 = 0;
                if (!DBNull.Value.Equals(row["candes"]))
                {
                    candes1 = row.Field<decimal>("candes");//cantidad guardada en campo candes al hacer grabar pendiente
                }

                if (cantid1 != 0) //si no toco al articulo, no le modifica su estdes ni su canfac
                {
                    //if (candes1 != 0) //si hace grabar pendiente en un articulo que estaba con grabar pendiente
                    //{
                    //    cantid1 = cantid1 + candes1;
                    //}

                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        f_actualizarRenglonesIngPar(codart1, codemp, numfac, cantidadDes, cantid1);
                    }
                }
            }

            if (lblError.Text.Trim() == "") //control de errores
            {
                Response.Redirect("w_TomaFisicaListado.aspx"); //ojo manejar error
            }
        }

        public void f_actualizarColoresGrv(string codemp, string numfac, string est001,
           string est002, string usu001)
        {
            //para actualizar los colores del enbabezadopedpro
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_COLOR_INGRESOS_ENC]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@est001", est001);
            cmd.Parameters.AddWithValue("@est002", est002);
            cmd.Parameters.AddWithValue("@usu001", usu001);
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*07. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }

        public void f_actualizarRenglonesIngPar(string codart1, string codemp, string numfac,
            decimal canfac, decimal candes)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_GRABAR_PENDIENTE_ING]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@canfac", canfac);
            cmd.Parameters.AddWithValue("@candes", candes);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*13. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }



        //***i PARA LLENAR INGRESOS**********************************************************

        //protected void btnConsolidar_Click(object sender, EventArgs e)
        //{
        //    if (Session["buyitems"] != null)
        //    {
        //        double r_Cantid, r_AuxCan, r_PorIva;
        //        int l_Filas, i, j, cont = 0;
        //        string s_CodArt = "", s_AuxArt, s_CodBus, s_NomArt, s_CodUni, s_CodIva, s_UbiFis, s_NumTra;
        //        DataTable dtConsolidar = (DataTable)Session["buyitems"];

        //        l_Filas = dtConsolidar.Rows.Count;
        //        for (i = 0; i <= l_Filas - 1; i++)
        //        {
        //            s_CodArt = dtConsolidar.Rows[i]["codart"].ToString();
        //            r_Cantid = double.Parse(dtConsolidar.Rows[i]["cantid"].ToString());
        //            s_NomArt = dtConsolidar.Rows[i]["nomart"].ToString();
        //            //s_UbiFis = dtConsolidar.Rows[i]["ubifis"].ToString();

        //            for (j = i + 1; j <= l_Filas - 1; j++)
        //            {
        //                s_AuxArt = dtConsolidar.Rows[j]["codart"].ToString();
        //                r_AuxCan = double.Parse(dtConsolidar.Rows[j]["cantid"].ToString());
        //                if (s_CodArt.Equals(s_AuxArt))
        //                {
        //                    dtConsolidar.Rows[j].Delete();
        //                    dtConsolidar.AcceptChanges();

        //                    l_Filas = l_Filas - 1;
        //                    j = j - 1;
        //                    r_Cantid = r_Cantid + r_AuxCan;

        //                    //break;
        //                }
        //            }
        //            dtConsolidar.Rows[i]["cantid"] = r_Cantid;
        //        }
        //        grvRenglonespedpro.DataSource = dtConsolidar;
        //        grvRenglonespedpro.DataBind();
        //        Session["buyitems"] = dtConsolidar;
        //    }            
        //}



        //public void f_insertarArticulos()
        //{
        //    lblError.Text = "";
        //    string codemp = Session["gs_CodEmp"].ToString();
        //    string numfac = f_CalcularSecuencia().Trim();
        //    string codpro = txtCodpro.Text;
        //    string codalm = Session["gs_CodAlm"].ToString();
        //    string codusu = Session["gs_CodUs1"].ToString(); //se inserta tambien en usuing
        //    string tiptra = Session["gs_Tiptar"].ToString();
        //    string numtra = Session["gs_Numtar"].ToString();
        //    //string codcli = Session["gs_Codcli"].ToString();
        //    //string nomcli = Session["gs_Nomcli"].ToString();            
        //    //string codven = Session["gs_CodVen"].ToString();            
        //    string sersec = Session["gs_SerSec"].ToString();
        //    string codsuc = Session["gs_CodSuc"].ToString();
        //    string s_enc = "";

        //    s_enc = f_llenarEncabezadoIngresos(codemp, numfac, codpro, codalm, codusu,
        //        tiptra, numtra, sersec, codsuc, codusu); //se llama al SP

        //    if (s_enc.Equals("")) //si no hubo error al insertar en el encabezado
        //    {
        //        DataTable dtArticulos = (DataTable)Session["buyitems"];
        //        long numren1 = 0;
        //        long numite1 = 0;
        //        foreach (DataRow row in dtArticulos.Rows)
        //        {
        //            //Session.Add("gs_Codart", row.Field<string>("codart"));
        //            string codart1 = row.Field<string>("codart");
        //            numren1 += 1;
        //            numite1 += 1;
        //            string nomart1 = row.Field<string>("nomart");
        //            string coduni1 = row.Field<string>("coduni");
        //            decimal preuni1 = decimal.Parse(row.Field<string>("preuni"));
        //            //string ubifis1 = row.Field<string>("ubifis");
        //            decimal cantid1 = decimal.Parse(row.Field<string>("cantid"));

        //            if (lblError.Text.Trim() == "") //control de errores
        //            {
        //                f_llenarRenglonesIngresos(codart1, codemp, numfac, numren1, numite1, nomart1, coduni1,
        //                preuni1, cantid1);
        //            }
        //        }
        //    }
        //}

        //public string f_llenarEncabezadoIngresos(string codemp, string numfac, string codpro,
        //    string codalm, string codusu, string tiptra, string numtra, string sersec,
        //    string codsuc, string usuing)
        //{
        //    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        //    conn.Open();

        //    SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_INGRESOS_ENC]", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@codemp", codemp);
        //    cmd.Parameters.AddWithValue("@numfac", numfac);
        //    cmd.Parameters.AddWithValue("@codpro", codpro);
        //    cmd.Parameters.AddWithValue("@codalm", codalm);
        //    cmd.Parameters.AddWithValue("@codusu", codusu);
        //    cmd.Parameters.AddWithValue("@tiptra", tiptra);
        //    cmd.Parameters.AddWithValue("@numtra", numtra);
        //    cmd.Parameters.AddWithValue("@sersec", sersec);
        //    cmd.Parameters.AddWithValue("@codsuc", codsuc);
        //    cmd.Parameters.AddWithValue("@usuing", usuing);
        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //        conn.Close();
        //        return ""; //se inserto correctamente
        //    }
        //    catch (Exception ex)
        //    {
        //        conn.Close();
        //        lblError.Text = "*04. " + DateTime.Now + ex.Message;
        //        return "*04. " + DateTime.Now + " " + ex.Message;
        //        //throw ex;
        //    }
        //}


        //public void f_llenarRenglonesIngresos(string codart1, string codemp, string numfac, long numren1,
        //    long numite1, string nomart1, string coduni1, decimal preuni1, decimal cantid1)
        //{
        //    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
        //    conn.Open();

        //    SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_INGRESOS_REN]", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@codart", codart1);
        //    cmd.Parameters.AddWithValue("@codemp", codemp);
        //    cmd.Parameters.AddWithValue("@numfac", numfac);
        //    cmd.Parameters.AddWithValue("@numren", numren1);
        //    cmd.Parameters.AddWithValue("@numite", numite1);
        //    cmd.Parameters.AddWithValue("@nomart", nomart1);
        //    cmd.Parameters.AddWithValue("@coduni", coduni1);
        //    cmd.Parameters.AddWithValue("@cantid", cantid1);
        //    cmd.Parameters.AddWithValue("@preuni", preuni1);
        //    //cmd.Parameters.AddWithValue("@ubifis", ubifis1);
        //    cmd.Parameters.AddWithValue("@codalm", Session["gs_CodAlm"].ToString());

        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //    }
        //    catch (Exception ex)
        //    {
        //        lblError.Text = "*05. " + DateTime.Now + ex.Message;
        //        //throw ex;
        //    }
        //    conn.Close();
        //}

        //***f PARA LLENAR INGRESOS**********************************************************


        //***i PARA PROVEEDORES**********************************************************
        //protected void grvProveedores_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    GridViewRow fila = grvProveedores.SelectedRow;
        //    string s_codpro = fila.Cells[1].Text.Trim();            
        //    string s_nompro = fila.Cells[2].Text.Trim();
        //    txtCodpro.Text = s_codpro;
        //    txtNompro.Text = s_nompro;
        //    lblError.Text = "";

        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$(function() { $('#modalProveedores').modal('hide'); });</script>", false);            
        //}

        //protected void btnBuscarNompro_Click(object sender, EventArgs e)
        //{
        //    if (txtBuscarNompro.Text.Equals(""))
        //    {
        //        Session["gs_Nompro"] = "%";
        //    }
        //    else
        //    {
        //        Session["gs_Nompro"] = txtBuscarNompro.Text.Trim();
        //    }

        //    ClientScript.RegisterStartupScript(this.GetType(), "Popup", "$('#modalProveedores').modal('show')", true);
        //}

        //protected void btnProveedores_Click(object sender, EventArgs e)
        //{
        //    Session.Add("gs_Nompro", "%");
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$(function() { $('#modalProveedores').modal('show'); });</script>", false);
        //}

        //***f PARA PROVEEDORES**********************************************************
    }
}