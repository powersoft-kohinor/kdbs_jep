﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjJEP
{
    public partial class w_Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) //para llenar ddlEmpresa
            {
                //Pasar info de sql data source a una tabla
                DataSourceSelectArguments args = new DataSourceSelectArguments();
                DataView view = (DataView)SqlDataSource1.Select(args); //procedimiento almacenado
                DataTable dt = view.ToTable();


                //llenar ddl con procedimiento almacenado
                dt.Columns.Add("ColumnaConTodo", typeof(string), "codemp + ' - ' + period + ' - ' + nomemp");
                ddlEmpresa.DataSource = dt;
                ddlEmpresa.DataTextField = "ColumnaConTodo";
                ddlEmpresa.DataValueField = "codemp";
                ddlEmpresa.DataBind();

                lblError.Visible = false;

                Session.Add("gs_CodEmp", dt.Rows[0].Field<string>("codemp"));
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_SEG_USUARIO_LOGIN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODEMP", ddlEmpresa.SelectedValue.ToString());
            cmd.Parameters.AddWithValue("@CODUS1", txtUsuario.Value);
            cmd.Parameters.AddWithValue("@CLAUSU", txtPassword.Value);
            //cmd.Parameters.AddWithValue("@CODUS1", "FERNANDO");
            //cmd.Parameters.AddWithValue("@CLAUSU", "JOSE170307");

            cmd.Parameters.Add("@s_CodBus", SqlDbType.Char, 10).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@gs_CodVen", SqlDbType.Char, 5).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@d_fecexp", SqlDbType.DateTime).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@gs_SerSec", SqlDbType.Char, 6).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@gs_CodSuc", SqlDbType.Char, 3).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@gs_CodAlm", SqlDbType.Char, 6).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@gs_UsuIng", SqlDbType.Char, 10).Direction = ParameterDirection.Output;

            cmd.ExecuteNonQuery();

            string s_codus1 = cmd.Parameters["@s_CodBus"].Value.ToString();
            //string s_codven = cmd.Parameters["@gs_CodVen"].Value.ToString();
            //string s_fecexp = cmd.Parameters["@d_fecexp"].Value.ToString();
            string s_sersec = cmd.Parameters["@gs_SerSec"].Value.ToString();
            string s_codsuc = cmd.Parameters["@gs_CodSuc"].Value.ToString();
            string s_codalm = cmd.Parameters["@gs_CodAlm"].Value.ToString();
            string s_usuing = cmd.Parameters["@gs_UsuIng"].Value.ToString();

            conn.Close();

            Session.Add("gs_UsuIng", s_usuing);
            Session.Add("gs_CodUs1", s_codus1);
            Session.Add("gs_SerSec", s_sersec);
            //Session.Add("gs_CodVen", s_codven);
            Session.Add("gs_CodAlm", s_codalm);
            Session.Add("gs_Codsuc", s_codsuc);            


            Session.Add("gs_CodEmp", ddlEmpresa.SelectedValue.ToString()); 

            if ((s_codus1).Trim().Equals("AAA"))
            {
                lblError.Text = "*Credenciales no concuerdan";
                lblError.Visible = true;
            }
            else
            {
                Response.Redirect("w_Inicio.aspx");
            }
        }
    }
}