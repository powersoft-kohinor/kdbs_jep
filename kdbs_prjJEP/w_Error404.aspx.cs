﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjJEP
{
    public partial class w_Error404 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //SE COMENTO !IPSPOSTBACK POR QUE SE ESTABA CARGANDO MAS DE UNA VEZ POR RAZONES DESCONOCIDAS
            if (!IsPostBack)
            {
                Session["gs_PagPre"] = Request.UrlReferrer.ToString(); //PARA OBTENER PAGINA PREVIA
            }
        }
        protected void lnbtnListadoDespacho_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_Inicio.aspx");
        }

        protected void lnbtnSalida_Click(object sender, EventArgs e)
        {
            //Response.Redirect("w_Salida.aspx");
            Response.Redirect("w_Login.aspx");
        }

        protected void lnbtnDespacho_Click(object sender, EventArgs e)
        {
            if (Session["gs_Numtra"] == null)
                Response.Write("<script>alert('Debe seleccionar un pedido');</script>");
            else
                Response.Redirect("w_Despacho.aspx");
        }

        protected void lnbtnTomaFisica_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TomaFisica.aspx");
        }

        protected void lnbtnImportListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_ImportListado.aspx");
        }

        protected void lnbtnImportaciones_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Importaciones primero.');</script>");
        }

        protected void lnbtnTransferListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TransferListado.aspx");
        }

        protected void lnbtnTransferencias_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Transferencias primero.');</script>");
        }

        protected void lkbtnVolver_Click(object sender, EventArgs e)
        {
            if (!Session["gs_PagPre"].ToString().Equals(""))            
                Response.Redirect(Session["gs_PagPre"].ToString());
        }
    }
}