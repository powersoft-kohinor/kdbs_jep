﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjJEP
{
    public partial class w_ImportListado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["gs_CodUs1"] != null)
                {
                    lblUsuario.Text = Session["gs_CodUs1"].ToString();
                    lblUsuarioDrop.Text = Session["gs_CodUs1"].ToString();
                }
            }
        }

        protected void lnbtnListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TomaFisicaListado.aspx");
        }

        protected void lnbtnTomaFisica_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Toma Física primero.');</script>");
        }

        protected void lnbtnSalida_Click(object sender, EventArgs e)
        {
            //Response.Redirect("w_Salida.aspx");
            Response.Redirect("w_Login.aspx");
        }

        protected void lnbtnListadoDespacho_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_Inicio.aspx");
        }

        protected void lnbtnDespacho_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Despacho primero.');</script>");
        }

        protected void lnbtnImportListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_ImportListado.aspx");
        }

        protected void lnbtnImportaciones_Click(object sender, EventArgs e)
        {
            if (Session["gs_Tittar"] == null)
                Response.Write("<script>alert('Debe seleccionar una tarea.');</script>");
            else
                Response.Redirect("w_ImportListado.aspx");
        }

        protected void lnbtnTransferListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TransferListado.aspx");
        }

        protected void lnbtnTransferencias_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Transferencias primero.');</script>");
        }


        protected void grvCrmTarea_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvCrmTarea.SelectedRow;
            string s_numtar = fila.Cells[2].Text.Trim();
            string s_tiptar = fila.Cells[5].Text.Trim();


            Session.Add("gs_Numtar", s_numtar.Trim()); //variable para que al seleccionar uno en el grv solo se añada ese a la dt
            Session.Add("gs_Tiptar", s_tiptar.Trim()); //variable para que al seleccionar uno en el grv solo se añada ese a la dt

            DataSourceSelectArguments args = new DataSourceSelectArguments(); //para pasar del SqlDataSource1 a una DataTable
            DataView view = (DataView)sqldsCrmTareaFull.Select(args);            //para pasar del SqlDataSource1 a una DataTable
            DataTable dt = view.ToTable(); //en esta dataTable esta el pedido seleccionado... (con los datos de este se llenara el encabezadoegresos)

            string tiptar = dt.Rows[0].Field<string>("tiptar");
            Session.Add("gs_Tiptar", tiptar.Trim());
            string numtar = dt.Rows[0].Field<string>("numtar");
            Session.Add("gs_Numtar", numtar.Trim());
            string tittar = dt.Rows[0].Field<string>("tittar");
            Session.Add("gs_Tittar", tittar);
            DateTime fectra = dt.Rows[0].Field<DateTime>("fectar");
            Session.Add("gs_Fectar", fectra);
            string numfac = dt.Rows[0].Field<string>("numfac");
            Session.Add("gs_Numfac", numfac.Trim());

            string est002 = "";
            if (dt.Rows[0].Field<string>("est002") != null) //para saber si est002 es NULL
            {
                est002 = dt.Rows[0].Field<string>("est002");
            }
            Session.Add("gs_Est002", est002.Trim());


            //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (AMARILLO)
            f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), numfac,
                "1", "", Session["gs_CodUs1"].ToString());

            Response.Redirect("w_Importaciones.aspx");
        }

        protected void grvCrmTarea_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            //para actualizar el color de las filas de grvEncabezadorpedpro
            foreach (GridViewRow row in grvCrmTarea.Rows)
            {
                if (row.Cells[6].Text.Trim().Equals("0")) //rojo
                {
                    row.BackColor = Color.LightCoral;
                }
                else
                {
                    row.BackColor = Color.Khaki; //amarillo
                    if (row.Cells[7].Text.Trim().Equals("1")) //verde
                    {
                        row.BackColor = Color.LightGreen;
                    }
                    else
                    {
                        if (row.Cells[7].Text.Trim().Equals("2")) //lila
                        {
                            row.BackColor = Color.Plum;
                        }
                        else
                        {
                            if (row.Cells[7].Text.Trim().Equals("3")) //azul
                            {
                                row.BackColor = Color.DodgerBlue;
                            }
                        }
                    }
                }
            }
            e.Row.Cells[6].Visible = false;
            e.Row.Cells[7].Visible = false;
        }

        public void f_actualizarColoresGrv(string codemp, string numfac, string est001,
           string est002, string usu001)
        {
            //para actualizar los colores del tarea import
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_COLOR_IMPORT_ENC]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@est001", est001);
            cmd.Parameters.AddWithValue("@est002", est002);
            cmd.Parameters.AddWithValue("@usu001", usu001);
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*" + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }



    }
}