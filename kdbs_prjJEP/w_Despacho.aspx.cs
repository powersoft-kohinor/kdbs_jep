﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

//LAST ERROR CODE: *16.

namespace kdbs_prjJEP
{
    public partial class w_TomaFisica : System.Web.UI.Page
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["gs_CodUs1"] != null)
                {
                    lblUsuario.Text = Session["gs_CodUs1"].ToString();
                    lblUsuarioDrop.Text = Session["gs_CodUs1"].ToString();
                }

                //PARA LLENAR grvRenglonespedpro
                DataSourceSelectArguments args = new DataSourceSelectArguments(); //para pasar del SqlDataSource1 a una DataTable
                DataView view = (DataView)sqldsRenglonespedpro.Select(args);            //para pasar del SqlDataSource1 a una DataTable
                DataTable dt = view.ToTable(); //en esta dataTable esta el pedido seleccionado... (con los datos de este se llenara el encabezadoegresos)
                //para agregar saldo (cantid original)
                dt.Columns.Add("saldo", typeof(decimal));
                foreach (DataRow row in dt.Rows)
                {
                    row["saldo"] = row.Field<decimal>("cantid"); //guarda el total de cada articulo en columna saldo

                    if (!DBNull.Value.Equals(row["canfac"])) //para saber si canfac es NULL
                    {
                        row["cantid"] = row.Field<decimal>("canfac"); //si han hecho despacho parcial el valor de canfac debe guardarse en cantid
                        row["saldo"] = row["cantid"];
                    }
                }

                Session.Add("gdt_Articulos", dt);
                grvRenglonespedpro.DataSource = dt;
                grvRenglonespedpro.DataBind();

                //la cantid puede ser 0 cuando hayan realizado un despacho parcial
                //para esto se llena el chk y se pinta de verde apenaz ingrese a Despacho.aspx
                int rowIndex = 0;
                foreach (GridViewRow row in grvRenglonespedpro.Rows)
                {
                    if ((int)decimal.Parse(row.Cells[2].Text.Trim()) == 0) //llena el chkConfirmar
                    {
                        CheckBox rBoton = (CheckBox)row.Cells[4].FindControl("chkConfirmado");
                        rBoton.Checked = true;
                        row.BackColor = Color.LightGreen;
                    }

                    //para poner color azul en caso de que haya sido grabado pendiente
                    string estdes = "";
                    if (!DBNull.Value.Equals(dt.Rows[rowIndex]["estdes"])) //si estdes no es NULL
                    {
                        estdes = dt.Rows[rowIndex].Field<string>("estdes");
                    }

                    if (estdes.Equals("1"))
                    {
                        row.BackColor = Color.DodgerBlue;
                    }

                    //PARA MOSTRAR EL CODIGO ALTERNO EN LUGAR DE CODART....SOLO SI NO ES NULL
                    if (row.Cells[5].Text != "&nbsp;") //para saber si desart es NULL
                    {
                        string s_aux = row.Cells[1].Text; //PARA PASAR desart A Cells[1] y codart  A Cells[5]
                        row.Cells[1].Text = row.Cells[5].Text; //Muestra desart en Código
                        row.Cells[5].Text = s_aux;
                    }
                    rowIndex++;
                }
                grvRenglonespedpro.Columns[5].Visible = false; //esconde la columna de desart

                txtFecegr.Text = DateTime.Now.ToString("yyyy-MM-dd");
                Session.Add("gs_ArtDespachado", "");
                Session.Add("gs_Admin", ""); //Para modo administrador
                Session.Add("gs_ContVal", "0"); //para que al aplastar muchas veces solo se haga 1

                //PARA CALCULAR CANTID TOTAL ...(PARA VALIDACION DE btnGuardar)                
                Session.Add("gs_CanTot", f_CanTot()); //variable que guarda la suma de las cantidades de grvRenglonespedpro

                //SECUENCIA N° EGR
                txtNumegr.Text = f_CalcularSecuencia();
            }

            if (IsPostBack) //PARA BUSCAR AL HACER TAB EN TXTBUSCAR
            {
                if (!txtBuscar.Text.Equals(""))
                {
                    //validacion para borrar 001 al final de codart...en caso de que tenga 001
                    string s_result = txtBuscar.Text;
                    try
                    {
                        string s_tresdigitos = txtBuscar.Text.Substring(txtBuscar.Text.Length - 4);
                        if (s_tresdigitos.Equals(" 001") || s_tresdigitos.Equals("0001"))
                            s_result = s_result.Substring(0, s_result.Length - 4);
                        else
                            s_result = txtBuscar.Text;
                    }
                    catch (Exception ex)
                    {
                        s_result = txtBuscar.Text;
                    }

                    clsArticulo objArticulo = f_BuscarArticulo(s_result);
                    f_ActualizarCampos(objArticulo);
                    txtBuscar.Focus();
                }
            }
        }
        protected void lnbtnListadoDespacho_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_Inicio.aspx");
        }

        protected void lnbtnDespacho_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_Despacho.aspx");
        }

        protected void lnbtnSalida_Click(object sender, EventArgs e)
        {
            //Response.Redirect("w_Salida.aspx");
            Response.Redirect("w_Login.aspx");
        }

        protected void lnbtnTomaFisica_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Toma Física primero.');</script>");
        }
        protected void lnbtnListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TomaFisicaListado.aspx");
            
        }

        protected void lnbtnImportListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_ImportListado.aspx");
        }

        protected void lnbtnImportaciones_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Importaciones primero.');</script>");
        }

        protected void lnbtnTransferListado_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TransferListado.aspx");
        }

        protected void lnbtnTransferencias_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('Acceso Denegado. Ir a Listado Transferencias primero.');</script>");
        }

        public void f_MostrarCodAlterno()
        {
            //grvRenglonespedpro.Columns[5].Visible = true;
            //PARA MOSTRAR EL CODIGO ALTERNO EN LUGAR DE CODART....SOLO SI NO ES NULL
            foreach (GridViewRow row in grvRenglonespedpro.Rows)
            {
                if (row.Cells[5].Text != "&nbsp;") //para saber si desart es NULL
                {
                    string s_aux = row.Cells[1].Text; //PARA PASAR desart A Cells[1] y codart  A Cells[5]
                    row.Cells[1].Text = row.Cells[5].Text; //Muestra desart en Código
                    row.Cells[5].Text = s_aux;
                }
            }
        }

        public int f_CanTot() //PARA CALCULAR CANTID TOTAL DE grvRenglonespedpro...(PARA VALIDACION DE btnGuardar)
        {
            int i_cantot = 0;
            foreach (GridViewRow row in grvRenglonespedpro.Rows)
            {
                i_cantot = i_cantot + (int)decimal.Parse(row.Cells[2].Text.Trim());
            }
            return i_cantot;
        }

        public string f_CalcularSecuencia()
        {
            //ANTES DE LLENAR ENCABEZADO INGRESOS SE CALCULA EL NUMFAC
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_SECUENCIA]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", Session["gs_CodEmp"].ToString());
            cmd.Parameters.AddWithValue("@codsec", "VC_EGR");
            cmd.Parameters.AddWithValue("@sersec", Session["gs_SerSec"].ToString()); //del usuario en Login

            cmd.Parameters.Add("@sp_NumCom", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;
            string s_numfac = "";
            try
            {
                cmd.ExecuteNonQuery();
                s_numfac = cmd.Parameters["@sp_NumCom"].Value.ToString();
            }
            catch (Exception ex)
            {
                lblError.Text = "*16. " + DateTime.Now +" "+ ex.Message;
                //throw ex;
            }
            conn.Close();

            Session.Add("gs_NumFac", s_numfac);
            return s_numfac;
        }

        public void f_ActualizarSecuencia()
        {
            //ACTUALIZA LA SECUENCIA CUANDO EL USUARIO SELECCIONA GUARDAR.
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_SEC2]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", Session["gs_CodEmp"].ToString());
            cmd.Parameters.AddWithValue("@codsec", "VC_EGR");
            cmd.Parameters.AddWithValue("@sersec", Session["gs_SerSec"].ToString()); //del usuario en Login

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*12. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }

        public clsArticulo f_BuscarArticulo(string s_codart)
        {
            clsArticulo objArticulo = new clsArticulo();
            DataTable dt = (DataTable)Session["gdt_Articulos"];

            if (s_codart.Any(char.IsDigit)) //si contiene por lo menos 1 numero (pistoleo el codigo....todo se hace automático)
            {
                try
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        string s_desart = "ZZZ"; //no poner vacio, ZZZ para que el al buscar nada en txtBuscar no haya problemas
                        if (!DBNull.Value.Equals(row["desart"])) //para saber si desart es NULL
                        {
                            s_desart = row.Field<string>("desart").Trim();
                        }

                        if ((row.Field<string>("codart").Trim().Equals(s_codart.Trim()) || s_desart.Equals(s_codart.Trim())) && (int)row.Field<decimal>("cantid") > 0)
                        {
                            objArticulo.Codart = s_codart;
                            string s_nomart = row.Field<string>("nomart");
                            objArticulo.Nomart = s_nomart;
                            int i_cantid = (int)row.Field<decimal>("cantid") - 1;

                            //actualiza el dt
                            row["cantid"] = i_cantid;

                            objArticulo.Cantid = i_cantid;
                            objArticulo.Error = "CORRECTO";

                            Session["gdt_Articulos"] = dt;
                            grvRenglonespedpro.Columns[5].Visible = true; //para q f_MostrarCodAlterno() funcione
                            grvRenglonespedpro.DataSource = dt;
                            grvRenglonespedpro.DataBind();

                            f_MostrarCodAlterno(); //PARA MOSTRAR CODIGO ALTERNO
                            f_pintarGridview(s_codart, i_cantid, s_desart);
                            grvRenglonespedpro.Columns[5].Visible = false; //ocultar columna extra...DEBE IR AQUI PARA QUE COLORES DE GRV FUNCIONEN
                            Session["gs_ArtDespachado"] = "1"; //para saber que ha despachado por lo menos 1 articulo..se usa en f_insertarArticulos
                            return objArticulo;
                        }
                    }
                }
                catch (Exception ex)
                {
                    objArticulo.Codart = "-";
                    objArticulo.Nomart = "-";
                    objArticulo.Cantid = 0;
                    objArticulo.Error = "*08. " + DateTime.Now + " " + ex.Message;
                    return objArticulo;
                }
            }
            else // si busco por nombre en txtBuscar
            {
                Session.Add("gs_Nomart", s_codart.Trim());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$(function() { $('#modalBuscar').modal('show'); });</script>", false);
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "";
                return objArticulo;
            }

            objArticulo.Codart = "-";
            objArticulo.Nomart = "-";
            objArticulo.Cantid = 0;
            objArticulo.Error = "*09. No se encontró artículo.";
            return objArticulo;
        }

        //*** INICIO CODIGO PARA BUSCAR CON WEBMETHOD EN LUGAR DE POSTBACK AL HACER TAB EN TXTBUSCAR**
        /*
        [WebMethod(EnableSession = true)] //permite obtener valores de variables session en el webmethod
        public static clsArticulo f_Validar(string s_codart, GridView grv)  //BUSCA datos al hacer tab en txtBuscar en w_TomaFisica
        {
            clsArticulo objArticulo = new clsArticulo();
            w_TomaFisica pg_TomaFisica = new w_TomaFisica(); //para poder llamar a Session en WebMethod
            DataTable dt = (DataTable)HttpContext.Current.Session["gdt_Articulos"];
            pg_TomaFisica.f_pintarGridview();

            if (s_codart.Any(char.IsDigit)) //si contiene por lo menos 1 numero (escribio el codigo)
            {

                try
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row.Field<string>("codart").Trim().Equals(s_codart.Trim()))
                        {
                            objArticulo.Codart = s_codart;
                            string s_nomart = row.Field<string>("nomart");
                            objArticulo.Nomart = s_nomart;
                            //decimal r_cantid = row.Field<decimal>("cantid");
                            int i_cantid = (int)row.Field<decimal>("cantid");
                            objArticulo.Cantid = i_cantid;
                            objArticulo.Error = "CORRECTO";
                            return objArticulo;
                        }
                    }                    
                }
                catch (Exception ex)
                {
                    objArticulo.Codart = "-";
                    objArticulo.Nomart = "-";
                    objArticulo.Cantid = 0;
                    objArticulo.Error = ex.Message;
                    return objArticulo;
                }
            }

            objArticulo.Codart = "-";
            objArticulo.Nomart = "-";
            objArticulo.Cantid = 0;
            objArticulo.Error = "No se encontro articulo";
            return objArticulo;
        }
        */
        //*** FIN CODIGO PARA BUSCAR CON WEBMETHOD EN LUGAR DE POSTBACK AL HACER TAB EN TXTBUSCAR**

        public void f_pintarGridview(string s_codart, int i_cantid, string s_desart) //pinta y llena el chkConfirmar
        {
            foreach (GridViewRow row in grvRenglonespedpro.Rows)
            {

                if ((int)decimal.Parse(row.Cells[2].Text.Trim()) == 0) //llena el chkConfirmar
                {
                    CheckBox rBoton = (CheckBox)row.Cells[4].FindControl("chkConfirmado");
                    rBoton.Checked = true;
                }
                string codart1 = row.Cells[1].Text.Trim();
                string alterno = row.Cells[5].Text.Trim();

                if (row.Cells[1].Text.Trim().Equals(s_codart.Trim()) || row.Cells[5].Text.Trim().Equals(s_codart.Trim())) //pinta la fila con codart
                {
                    if (i_cantid == 0)
                        row.BackColor = Color.LightGreen;
                    else
                        row.BackColor = Color.Khaki;
                }
            }
        }

        protected void grvArticulos_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow fila = grvArticulos.SelectedRow;
            string s_codart = fila.Cells[1].Text.Trim();
            string s_cantid = Server.HtmlDecode(fila.Cells[2].Text.Trim()); //para que acepte caracteres especiales (Ñ)
            string s_nomart = fila.Cells[3].Text.Trim();
            txtCodart.Text = s_codart;
            txtCantid.Text = "1";
            txtSaldo.Text = s_cantid;
            txtNomart.Text = s_nomart;
            lblError.Text = "";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$(function() { $('#modalBuscar').modal('hide'); });</script>", false);
        }

        protected void btnBuscarNomart_Click(object sender, EventArgs e)
        {
            if (txtBuscarNomart.Text.Equals(""))
            {
                Session["gs_Nomart"] = "%";
            }
            else
            {
                Session["gs_Nomart"] = txtBuscarNomart.Text.Trim();
            }

            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "$('#modalBuscar').modal('show')", true);
        }

        protected void btnDespachar_Click(object sender, EventArgs e)
        {
            clsArticulo objArticulo = new clsArticulo();
            try
            {
                objArticulo.Codart = txtCodart.Text;
                objArticulo.Nomart = txtNomart.Text;
                objArticulo.Cantid = (int)decimal.Parse(txtSaldo.Text);
                objArticulo.Error = "";
                DataTable dt = (DataTable)Session["gdt_Articulos"];
                string s_codart = txtCodart.Text.Trim();

                int i_cantidDesapachar = 1;
                if (!txtCantid.Text.Equals("")) //si deja vacio txtCantid se asume que es 1
                {
                    i_cantidDesapachar = int.Parse(txtCantid.Text.ToString());
                }

                foreach (DataRow row in dt.Rows)
                {
                    string s_desart = "ZZZ"; //no poner vacio, ZZZ para que el al buscar nada en txtBuscar no haya problemas
                    if (!DBNull.Value.Equals(row["desart"])) //para saber si desart es NULL
                    {
                        s_desart = row.Field<string>("desart").Trim();
                    }

                    if ((row.Field<string>("codart").Trim().Equals(s_codart.Trim()) || s_desart.Equals(s_codart.Trim())) && (int)row.Field<decimal>("cantid") > 0)
                    {
                        if ((int)row.Field<decimal>("cantid") < i_cantidDesapachar) //si ingresa cantidad mayor que saldo existente
                        {
                            txtCantid.Text = "1";
                            objArticulo.Error = "*10. Cantidad ingresada es superior a Saldo. Despacho no realizado.";
                            f_ActualizarCampos(objArticulo);
                        }
                        else //cantidad correcta
                        {
                            objArticulo.Codart = s_codart;
                            string s_nomart = row.Field<string>("nomart");
                            objArticulo.Nomart = s_nomart;
                            int i_cantid = (int)row.Field<decimal>("cantid") - i_cantidDesapachar;

                            //actualiza el dt
                            row["cantid"] = i_cantid;

                            objArticulo.Cantid = i_cantid;
                            objArticulo.Error = "CORRECTO";

                            Session["gdt_Articulos"] = dt;
                            grvRenglonespedpro.Columns[5].Visible = true; //para q f_MostrarCodAlterno() funcione
                            grvRenglonespedpro.DataSource = dt;
                            grvRenglonespedpro.DataBind();

                            f_MostrarCodAlterno(); //PARA MOSTRAR CODIGO ALTERNO
                            f_pintarGridview(s_codart, i_cantid, s_desart);
                            grvRenglonespedpro.Columns[5].Visible = false; //ocultar columna extra...DEBE IR AQUI PARA QUE COLORES DE GRV FUNCIONEN
                            f_ActualizarCampos(objArticulo);
                        }
                    }
                }
                //aqui poner objArticulo.Codart = "-"; .....
            }
            catch (Exception ex)
            {
                objArticulo.Codart = "-";
                objArticulo.Nomart = "-";
                objArticulo.Cantid = 0;
                objArticulo.Error = "*11. " + DateTime.Now + " " + ex.Message;
                f_ActualizarCampos(objArticulo);
            }
        }

        public void f_ActualizarCampos(clsArticulo objArticulo)
        {
            txtCodart.Text = objArticulo.Codart;
            txtNomart.Text = objArticulo.Nomart;
            txtSaldo.Text = objArticulo.Cantid.ToString();
            if (!objArticulo.Error.Equals("CORRECTO"))
            {
                lblError.Text = "*01. " + DateTime.Now + " " + objArticulo.Error;
            }
            else
            {
                lblError.Text = "";
            }
            txtBuscar.Text = "";
            //txtBuscarMobile.Text = "";
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (f_CanTot() == 0) //verifica si todos los cantid estan en 0
            {
                if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
                {
                    Session["gs_ContVal"] = "1";
                    f_insertarArticulos("btnGuardar", "1"); //est002=1 ...para q wladi sepa si fue parcial o completo

                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        f_actualizarEncabezadopedpro(Session["gs_CodEmp"].ToString(),
                            Session["gs_Numtra"].ToString(), txtNumegr.Text);
                    }

                    //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (VERDE)
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numtra"].ToString(),
                            "1", "1", Session["gs_CodUs1"].ToString());
                    }

                    //redireccion despues de guardar
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        Response.Redirect("w_Inicio.aspx");
                        //Response.Write("<script>alert('Todos los totales deben estar llenos');</script>"); //OJO NO VALE PONER RESPONSE .REDIRECT Y RESPONSE.WRITE JUNTOS....EL ALTERT NO SE ACTIVA
                    }
                }
            }
            else
            {
                lblError.Text = "*02. "+ DateTime.Now + " " + "Debe despachar todos los artículos antes de Guardar Completo.";
            }
        }

        protected void btnGuardarParcial_Click(object sender, EventArgs e)
        {
            lblAlertaModal.Text = "";
            int rowIndex = 0;
            DataTable dtObservacionArticulos = (DataTable)Session["gdt_Articulos"];

            //PARA MOSTRAR ROW DE LOS ARTICULOS QUE NO SE VEN A INSERTAR (SU CANTID NO ES CERO)
            //for (int i = 0; i < dtObservacionArticulos.Rows.Count; i++)
            //{
            //    if (cantidadDes == 0) //si sobran articulos los muestra (si su cantidadDes es 0)
            //    {
            //        dtObservacionArticulos.Rows.RemoveAt(i);
            //    }
            //}

            grvObservacionArticulos.Columns[2].Visible = true;
            grvObservacionArticulos.DataSource = dtObservacionArticulos;
            grvObservacionArticulos.DataBind();

            foreach (GridViewRow row in grvObservacionArticulos.Rows)
            {
                //PARA MOSTRAR EL CODIGO ALTERNO EN LUGAR DE CODART....SOLO SI NO ES NULL
                if (row.Cells[2].Text != "&nbsp;") //para saber si desart es NULL
                {
                    string s_aux = row.Cells[1].Text; //PARA PASAR desart A Cells[1] y codart  A Cells[5]
                    row.Cells[1].Text = row.Cells[2].Text; //Muestra desart en Código
                    row.Cells[2].Text = s_aux;
                }

                //PARA MOSTRAR ROW DE LOS ARTICULOS QUE NO SE VEN A INSERTAR (SU CANTID NO ES CERO)
                decimal cantidadDes = dtObservacionArticulos.Rows[rowIndex].Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                //decimal saldo = dt.Rows[rowIndex].Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                //decimal cantid1 = saldo - cantidadDes; //articulos despachados

                if (cantidadDes != 0) //si sobran articulos los muestra (si su cantidadDes no es 0)
                {
                    grvObservacionArticulos.Rows[rowIndex].Visible = true;
                }
                else
                {
                    grvObservacionArticulos.Rows[rowIndex].Visible = false;
                }
                rowIndex++;
            }

            grvObservacionArticulos.Columns[2].Visible = false; //esconde la columna de desart
            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "$('#modalObservacion').modal('show')", true);
            //de aqui va al btnAceptar
        }

        public void f_insertarArticulos(string s_tipoGuargar, string est002) //metodo para insertar enc y ren egresos
        {
            lblError.Text = "";
            string numfac = "";
            //if (Session["gs_Admin"].ToString().Equals("1"))            
            //    numfac = txtNumegr.Text;
            //else
            //    numfac = f_CalcularSecuencia().Trim();
            numfac = f_CalcularSecuencia().Trim();
            string codemp = Session["gs_CodEmp"].ToString();
            string codcli = Session["gs_Codcli"].ToString();
            string nomcli = Session["gs_Nomcli"].ToString();
            string codalm = Session["gs_CodAlm"].ToString();
            string codven = Session["gs_CodVen"].ToString();
            string codusu = Session["gs_CodUsu"].ToString();
            string sersec = Session["gs_SerSec"].ToString();
            string codsuc = Session["gs_CodSuc"].ToString();
            string tiptra = Session["gs_Tiptra"].ToString();
            string numtra = Session["gs_Numtra"].ToString();
            string usuing = Session["gs_CodUs1"].ToString();
            string observ = "";
            if (Session["gs_ObservEncPed"] != null)
                observ = Session["gs_ObservEncPed"].ToString();

            //string coddes = "";
            //string coddes = ddlObservacion.SelectedValue.ToString();
            string s_enc = "";

            if (Session["gs_ArtDespachado"].ToString().Equals("1")) //inserta si despacho por lo menos 1 articulo
            {
                s_enc = f_llenarEncabezadoEgresos(codemp, numfac, codcli, codalm,
                codven, codusu, nomcli, tiptra, numtra, sersec, codsuc, usuing, observ, est002); //se llama al SP
            }
            else
            {
                if (Session["gs_Est002"].ToString().Equals("3")) //inserta si haybia hecho desp parcial o grabar pendiente
                {
                    s_enc = f_llenarEncabezadoEgresos(codemp, numfac, codcli, codalm,
                codven, codusu, nomcli, tiptra, numtra, sersec, codsuc, usuing, observ, est002); //se llama al SP
                }
                else
                {
                    lblError.Text = "*15. " + DateTime.Now + "Debe despachar por lo menos 1 artículo para poder guardar.";
                }
            }

            if (s_enc.Equals("")) //si no hubo error al insertar en el encabezado
            {
                DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];
                int rowIndex = 0; //para enconctrar ddl en grvObservacion                
                long numren1 = 0;
                long numite1 = 0;
                foreach (DataRow row in dtArticulos.Rows)
                {                   
                    Session.Add("gs_Codart", row.Field<string>("codart"));
                    string codart1 = row.Field<string>("codart");
                    numren1 += 1;
                    numite1 += 1;
                    string nomart1 = row.Field<string>("nomart");
                    string coduni1 = row.Field<string>("coduni");
                    decimal preuni1 = row.Field<decimal>("preuni");
                    string observ1 = "";
                    string estdes1 = "";
                    if (!DBNull.Value.Equals(row["estdes"])) //para saber si estdes es NULL
                    {
                        estdes1 = row.Field<string>("estdes");
                    }

                    decimal cantidadDes = row.Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                    decimal saldo = row.Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                    decimal cantid1 = saldo - cantidadDes; //articulos despachados

                    decimal candes1 = 0;
                    if (!DBNull.Value.Equals(row["candes"]))
                    {
                        candes1 = row.Field<decimal>("candes");//cantidad guardada en campo candes al hacer grabar pendiente
                    }

                    //***para obtener observ de grvObservacion al hacer clic en btnAceptar
                    if (s_tipoGuargar.Equals("btnAceptar")) //si se hizo clic en btnAceptar
                    {
                        DropDownList ddlgrvObserv = (DropDownList)grvObservacionArticulos.Rows[rowIndex].Cells[3].FindControl("ddlgrvObservacion");
                        if (cantidadDes != 0) //si sobran articulos los muestra (si su cantidadDes no es 0)
                        {
                            observ1 = ddlgrvObserv.SelectedValue.ToString().Trim();
                        }                                               
                    }

                    if (cantid1 != 0) //si no toco al articulo, no lo inserta en la tabla (si su cantid1 es 0)
                    {
                        if (candes1 != 0) //si hace despacho parcial en un articulo que estaba con grabar pendiente
                        {
                            cantid1 = cantid1 + candes1;
                        }
                        if (lblError.Text.Trim() == "") //control de errores
                        {
                            f_llenarRenglonesEgresos(codart1, codemp, numfac, numren1, numite1, nomart1, coduni1,
                                preuni1, cantid1, cantidadDes, observ1);
                        }
                        decimal totven = preuni1 * cantid1;
                        totven = Math.Round(totven, 2); //para 2 decimales
                        if (lblError.Text.Trim() == "") //control de errores
                        {
                            f_llenarKardex(codemp, numfac, codart1, numren1, coduni1, codalm, cantid1, codcli, codven,
                                codusu, nomcli, tiptra, numtra, sersec, codsuc, totven, preuni1); //OJO totven ya no se usa para INSERT
                        }
                    }
                    else
                    {                       
                        if (estdes1.Equals("1") && candes1 != 0) // si fue grabado pendiente
                        {
                            if (lblError.Text.Trim() == "") //control de errores
                            {
                                f_llenarRenglonesEgresos(codart1, codemp, numfac, numren1, numite1, nomart1, coduni1,
                                    preuni1, candes1, cantidadDes, observ1);
                            }
                            decimal totven = preuni1 * cantid1;
                            totven = Math.Round(totven, 2); //para 2 decimales
                            if (lblError.Text.Trim() == "" && !(codart1[0].ToString().Equals("/"))) //control de errores... si es servicio de transporte (/V400) no lo inserta
                            {
                                f_llenarKardex(codemp, numfac, codart1, numren1, coduni1, codalm, candes1, codcli, codven,
                                    codusu, nomcli, tiptra, numtra, sersec, codsuc, totven, preuni1); //OJO totven ya no se usa para INSERT
                            }
                        }
                    }
                    rowIndex++;
                    
                    if (lblError.Text.Trim() == "" && !(observ1.Equals(""))) //control de errores
                    {
                        f_actualizarObsev(codart1,codemp,numtra,observ1); //para actualizar las observ
                    }
                }
            }
        }

        public string f_llenarEncabezadoEgresos(string codemp, string numfac, string codcli,
            string codalm, string codven, string codusu, string nomcli, string tiptra,
            string numtra, string sersec, string codsuc, string usuing, string observ, string est002)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_EGRESOS_ENC]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@codcli", codcli);
            cmd.Parameters.AddWithValue("@codalm", codalm);
            cmd.Parameters.AddWithValue("@codven", codven);
            cmd.Parameters.AddWithValue("@codusu", codusu);
            cmd.Parameters.AddWithValue("@nomcli", nomcli);
            cmd.Parameters.AddWithValue("@tiptra", tiptra);
            cmd.Parameters.AddWithValue("@numtra", numtra);
            cmd.Parameters.AddWithValue("@sersec", sersec);
            cmd.Parameters.AddWithValue("@codsuc", codsuc);
            cmd.Parameters.AddWithValue("@usuing", usuing);
            cmd.Parameters.AddWithValue("@observ", observ);
            cmd.Parameters.AddWithValue("@est002", est002);

            try
            {
                cmd.ExecuteNonQuery();
                conn.Close();

                //actualiza la secuencia
                f_ActualizarSecuencia();
                return ""; //se inserto correctamente
            }
            catch (Exception ex)
            {
                conn.Close();
                lblError.Text = "*03. " + DateTime.Now + " " + ex.Message;
                return "*03. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
        }

        public void f_llenarRenglonesEgresos(string codart1, string codemp, string numfac, long numren1,
            long numite1, string nomart1, string coduni1, decimal preuni1, decimal cantid1,
            decimal canfac, string observ)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_EGRESOS_REN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numfac", numfac);
            cmd.Parameters.AddWithValue("@numren", numren1);
            cmd.Parameters.AddWithValue("@numite", numite1);
            cmd.Parameters.AddWithValue("@nomart", nomart1);
            cmd.Parameters.AddWithValue("@coduni", coduni1);
            cmd.Parameters.AddWithValue("@cantid", cantid1);
            cmd.Parameters.AddWithValue("@preuni", preuni1);
            //cmd.Parameters.AddWithValue("@ubifis", ubifis1);
            cmd.Parameters.AddWithValue("@codalm", Session["gs_CodAlm"].ToString());
            cmd.Parameters.AddWithValue("@numtra", Session["gs_Numtra"].ToString());
            cmd.Parameters.AddWithValue("@canfac", canfac);
            cmd.Parameters.AddWithValue("@observ", observ);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*04. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }

        public void f_llenarKardex(string codemp1, string numfac, string codart1, long numren1, string coduni1, string codalm,
             decimal cantid1, string codcli, string codven, string codusu, string nomcli, string tiptra, string numtra,
             string sersec, string codsuc, decimal totven, decimal preuni1)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_KARDEX]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp1);
            cmd.Parameters.AddWithValue("@numdoc", numfac);
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@numren", numren1);
            cmd.Parameters.AddWithValue("@coduni", coduni1);
            cmd.Parameters.AddWithValue("@codalm", codalm);
            cmd.Parameters.AddWithValue("@cantid", cantid1);
            cmd.Parameters.AddWithValue("@codcli", codcli);
            cmd.Parameters.AddWithValue("@codven", codven);
            cmd.Parameters.AddWithValue("@codusu", codusu);
            cmd.Parameters.AddWithValue("@nomcli", nomcli);
            cmd.Parameters.AddWithValue("@tiptra", tiptra);
            cmd.Parameters.AddWithValue("@numtra", numtra);
            cmd.Parameters.AddWithValue("@sersec", sersec);
            cmd.Parameters.AddWithValue("@codsuc", codsuc);
            cmd.Parameters.AddWithValue("@totven", totven);
            cmd.Parameters.AddWithValue("@preuni", preuni1);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*05. " + DateTime.Now + " " + ex.Message;
                //throw;
            }
            conn.Close();
        }

        public void f_actualizarEncabezadopedpro(string codemp, string numtra, string numfac)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_PEDPRO_ENC]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numtra", numtra);
            cmd.Parameters.AddWithValue("@numfac", numfac);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*06. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }

        protected void btnAceptar_Click(object sender, EventArgs e) //para guardarParcial
        {
            if (f_CanTot() == 0) //verifica si todos los cantid estan en 0....REALIZO PARCIAL PERO TODOS ESTABAN EN CERO....SE PONE EN VERDE
            {
                if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
                {
                    Session["gs_ContVal"] = "1";
                    f_insertarArticulos("btnAceptar", "1"); //est002=1 ...para q wladi sepa si fue parcial o completo

                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        f_actualizarEncabezadopedpro(Session["gs_CodEmp"].ToString(),
                            Session["gs_Numtra"].ToString(), txtNumegr.Text);
                    }

                    //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (VERDE)
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numtra"].ToString(),
                            "1", "1", Session["gs_CodUs1"].ToString());
                    }

                    //redireccion despues de guardar
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        Response.Redirect("w_Inicio.aspx");
                    }
                }                
            }
            else //GRABADO PARCIAL NORMAL
            {
                if (Session["gs_ContVal"].ToString().Equals("0")) //para que al aplastar muchas veces solo se haga 1
                {
                    Session["gs_ContVal"] = "1";
                    f_insertarArticulos("btnAceptar", "2"); //est002=2 ...para q wladi sepa si fue parcial o completo

                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        f_actualizarEncabezadopedpro(Session["gs_CodEmp"].ToString(),
                            Session["gs_Numtra"].ToString(), txtNumegr.Text);
                    }

                    //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (LILA)
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        f_actualizarColoresGrv(Session["gs_CodEmp"].ToString(), Session["gs_Numtra"].ToString(),
                            "1", "2", Session["gs_CodUs1"].ToString());
                    }

                    //redireccion despues de guardar
                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        Response.Redirect("w_Inicio.aspx");
                    }
                }
            }
        }

        public void f_actualizarColoresGrv(string codemp, string numtra, string est001,
            string est002, string usu001)
        {
            //para actualizar los colores del enbabezadopedpro
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_COLOR_PEDPRO_ENC]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numtra", numtra);
            cmd.Parameters.AddWithValue("@est001", est001);
            cmd.Parameters.AddWithValue("@est002", est002);
            cmd.Parameters.AddWithValue("@usu001", usu001);
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*07. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }

        protected void btnPendiente_Click(object sender, EventArgs e) //para que despachen cierta cantidad y se guarde al volver a ingresar (NO INSERTA EN TBLS...solo hace update)
        {
            string codemp = Session["gs_CodEmp"].ToString();
            string numtra = Session["gs_Numtra"].ToString();

            //PARA ACTUALIZAR COLORES DE GRVENCABEZADOPEDPRO (AZUL)
            if (lblError.Text.Trim() == "") //control de errores
            {
                f_actualizarColoresGrv(codemp, numtra,
                    "1", "3", Session["gs_CodUs1"].ToString());
            }

            DataTable dtArticulos = (DataTable)Session["gdt_Articulos"];
            foreach (DataRow row in dtArticulos.Rows)
            {
                Session.Add("gs_Codart", row.Field<string>("codart"));
                string codart1 = row.Field<string>("codart");
                //string nomart1 = row.Field<string>("nomart");
                //string coduni1 = row.Field<string>("coduni");
                //decimal preuni1 = row.Field<decimal>("preuni");  

                decimal cantidadDes = row.Field<decimal>("cantid"); //cantidad de total de articulos menos los q se despacho (canfac)
                decimal saldo = row.Field<decimal>("saldo"); //cantidad total de articulo con el que se empezo
                decimal cantid1 = saldo - cantidadDes; //articulos despachados

                //NUEVO CODIGO 20200217 PARA QUE AL GRABAR PENDIENTE 2 VECES EL MISMO ARTICULO SUME LAS CANDES
                decimal candes1 = 0;
                if (!DBNull.Value.Equals(row["candes"]))
                {
                    candes1 = row.Field<decimal>("candes");//cantidad guardada en campo candes al hacer grabar pendiente
                }

                if (cantid1 != 0) //si no toco al articulo, no le modifica su estdes ni su canfac
                {
                    if (candes1 != 0) //si hace grabar pendiente en un articulo que estaba con grabar pendiente
                    {
                        cantid1 = cantid1 + candes1;
                    }

                    if (lblError.Text.Trim() == "") //control de errores
                    {
                        f_actualizarRenglonespedpro(codart1, codemp, numtra, cantidadDes, cantid1);
                    }
                }
            }

            if (lblError.Text.Trim() == "") //control de errores
            {
                Response.Redirect("w_Inicio.aspx"); //ojo manejar error
            }
        }

        public void f_actualizarRenglonespedpro(string codart1, string codemp, string numtra,
            decimal canfac, decimal candes)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_M_GRABAR_PENDIENTE]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numtra", numtra);
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@canfac", canfac);
            cmd.Parameters.AddWithValue("@candes", candes);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*13. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }

        public void f_actualizarObsev(string codart1, string codemp, string numtra, string observ)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kdbs_EsperanzaConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("[dbo].[W_CRM_ACTUALIZAR_PEDPRO_REN]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codemp", codemp);
            cmd.Parameters.AddWithValue("@numtra", numtra);
            cmd.Parameters.AddWithValue("@codart", codart1);
            cmd.Parameters.AddWithValue("@observ", observ);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                lblError.Text = "*14. " + DateTime.Now + " " + ex.Message;
                //throw ex;
            }
            conn.Close();
        }

        protected void lkbtnAdmin_Click(object sender, EventArgs e)
        {
            //Response.Redirect("w_Salida.aspx");
            Response.Redirect("w_Login.aspx");
        }
    }
}


