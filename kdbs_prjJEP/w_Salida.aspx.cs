﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kdbs_prjJEP
{
    public partial class w_Salida : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lnbtnDespacho_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_Inicio.aspx");
        }

        protected void lnbtnSalida_Click(object sender, EventArgs e)
        {
            //Response.Redirect("w_Salida.aspx");
            Response.Redirect("w_Login.aspx");
        }

        protected void lnbtnTomaFisica_Click(object sender, EventArgs e)
        {
            Response.Redirect("w_TomaFisica.aspx");
        }
    }
}