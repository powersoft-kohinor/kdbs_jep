# KOHINOR MOVIL JEP IMPORTACIONES #

Aplicacion para control de inventario: ingresos, egresos, trasferencias de bodega, importaciones.

### Sobre el Proyecto ###

* ASP.NET Framework 4
* Web Frorms
* App web pensada para uso dispositivos moviles, responsiva
* Version 1.0.0

### Uso ###

* Clonar repo y ejecutar en Visual Studio. Git requerido en m�quina local.
* Base de datos SQL Server: kdbs_jep

### Configuraci�n IIS ###

1. INSTALAR IIS DENTRO DEL ADMINISTRADOR DEL SERVIDOR
    * AGREGAR ROLES Y CARACTERISTICAS (TODO NEXT)
    * INSTALAR TODOS LOS SERVICIOS DE IIS....EN SERVICIOS DE ROL
        * -->INSTALAR TODOS LOS SERVICIOS HTTP, CASO CONTRARIO NO SE RENDERIZARA EL CSS, IMAGENES, JQUERY, ETC.  
      

2. INSTALAR [WEB DEPLOY 3.6](https://www.iis.net/downloads/microsoft/web-deploy)
    * INSTALAR TODAS LAS OPCIONES...HASTA LAS QUE ASOMAN CON X
    

3. ABRIR ADMINISTRADOR DE IIS Y CONFIGURAR:
    * EN GRUPOS DE APLICACIONES -->DEFAULT POOL -->CONFIG AVANZADA
        * -->IDENTIDAD: PONER EL USUARIO Y SU CONSTRASE�A (WIN-KJHY7W/Administrador)
        * -->REVISAR QUE LA VERSION DEL DEFAULT POOL SEA V4.0
    * EN DEFAULT WEBSITE -->CONFIG AVANZADA -->CREDENCIALES DE RUTA DE ACCESO
        * -->PONER EL USUARIO Y SU CONSTRASE�A (WIN-KJHY7W/Administrador)
    * EN DOCUMENTO PREDETERMINADO -->AGREGAR -->PONER NOMBRE DE PAG PRINCIPAL (w_Login.aspx)
    
     
4. EN ADMINISTRADOR DE IIS--> DEFAULT WEB APPLICATION -->CLIC DERECHO Y DEPLOY (IMPLEMENTAR)
    * EN GRUPOS DE APLICACIONES -->DEFAULT POOL -->CONFIG AVANZADA
        * -->LUEGO PONER CONFIGURAR (HABILITAR PUBLICACION DE WEB DEPLOY) PARA GENERAR EL ARCHIVO EN EL DIRECTORIO INDICADO
        * COPIAR ARCHIVO Y PASARLO AL PC CON VISUAL STUDIO [LINK](https://docs.microsoft.com/en-us/visualstudio/deployment/tutorial-import-publish-settings-iis?view=vs-2019)
        * EN CASO DE QUE NO HAYA OPCION DE DEPLOY [LINK](https://stackoverflow.com/questions/41386690/missing-import-web-application-option-in-web-deploy-3-6)
        

5. EN VS, CLIC DERECHO EN PRJ Y PUBLICAR
    * IMPORTAR EL ARCHIVO QUE SE OBTUBO DEL SERVIDOR
    * PONER CONTRASE�A DE USUARIO DEL SERVIDOR
    * CAMBIAR EL NOMBRE POR LA IP DEL SERVIDOR EN CAMPOS Server: Y Destination URL:
    * VERIFICAR CONEXION Y SUBIR PROYECTO


6. INSTALAR EL FRAMEWORK RESPECTIVO DEL PROYECTO EN EL SERVIDOR


7. PARA ERRORES DE PUBLICACION  
https://stackoverflow.com/questions/11796838/web-deployment-task-failed-could-not-connect-server-did-not-respond

8. EN CASO DE REQUERIR INSTALAR .NET FRAMEWORK 4.7.2  
https://support.microsoft.com/en-us/help/4054542/microsoft-net-framework-4-7-2-for-windows-server-2012


9. PARA ERRORES DE CONEXION REMOTA DE SQL  
https://support.timextender.com/hc/en-us/articles/360042584612-Enable-Remote-Connections-to-SQL-Server-using-IP-address  
https://www.sqlshack.com/es/como-conectarse-a-un-sql-server-remoto/